"""Model summary tools

This script contains tools for summarizing a SM model.

Parameters
----------
--tool: str
    two type of summary tools are supported. default=text_summary
        text_summary: summary tool for parsing model and getting default chromosome
        graph_summary: summary tool for showing the node relationship in a graph view

--model_path: str
    model configuration file path
--resource_config: str
    resource configuration file path
-v: int
    '--verbose': (optional) default=0
    set the logging level
        0: no logging (critical error only)
        1: info level
        2: debug level

Example
-------
## Graph summary tool

### Python API example
```
import simplemind.sm_model_summary

model_path = f'/radraid/apps/personal/youngwon/debug_miu/cxr_model.9/et_tip_placement_model'

log = logging.getLogger()
formatter = logging.Formatter('[%(asctime)s|%(name)-10s|%(levelname)-8s|%(filename)-25s:%(lineno)-3s] %(message)s')
ch = logging.StreamHandler()
ch.setFormatter(formatter)
log.addHandler(ch)
log.setLevel(logging.DEBUG)

sm_model = SMBaseModel(model_path, log)
print(sm_model.get_list_relationship())

G, fig = sm_model.get_summary_graph(figsize=(5,10), font_size=12)
fig.show()
```

### command-line usage
```
export sm_model_summary=/cvib2/apps/personal/youngwonchoi/SM/simplemind/simplemind/sm_model_summary.py 
export model_path=/radraid/apps/personal/youngwon/debug_miu/cxr_model.9/et_tip_placement_model

python $sm_model_summary --tool=graph_summary --model_path=$model_path
```

## Text summary tool

### command-line usage

Without saving file
```
export sm_model_summary=/cvib2/apps/personal/youngwonchoi/SM/simplemind/simplemind/sm_model_summary.py 
export model_path=/radraid/apps/personal/youngwon/miu_prostate_organ/public_promise12/ga_experiments/ga_promise12_v1/miu_model_ga_promise12_v1/prostate_model

python $sm_model_summary --model_path=$model_path
```

Save the summary file too
```
export sm_model_summary=/cvib2/apps/personal/youngwonchoi/SM/simplemind/simplemind/sm_model_summary.py 
export model_path=/radraid/apps/personal/youngwon/miu_prostate_organ/public_promise12/ga_experiments/ga_promise12_v1/miu_model_ga_promise12_v1/prostate_model
export summary_path=/radraid/apps/personal/youngwon/miu_prostate_organ/public_promise12/ga_experiments/ga_prostate_v1/miu_model_ga_promise12_v1_summary.yml

python $sm_model_summary --model_path=$model_path --summary_path=$summary_path
```

Save the summary file too
```
export sm_model_summary=/cvib2/apps/personal/youngwonchoi/SM/simplemind/simplemind/sm_model_summary.py 
export model_path=/radraid/apps/personal/youngwon/miu_prostate_organ/public_promise12/ga_experiments/ga_promise12_v1/miu_model_ga_promise12_v1/prostate_model
export summary_path=/radraid/apps/personal/youngwon/miu_prostate_organ/public_promise12/ga_experiments/ga_prostate_v1/miu_model_ga_promise12_v1_summary.yml

python $sm_model_summary --model_path=$model_path --summary_path=$summary_path
```

analyze with a specific binary chromosome
```
export sm_model_summary=/cvib2/apps/personal/youngwonchoi/SM/simplemind/simplemind/sm_model_summary.py 
export model_path=/radraid/apps/all/seg_model/cxr_trachea_model.1/cxr_trachea_model
export summary_path=/radraid/apps/personal/youngwon/debuging_ga/summary_cxr_model.yml
export binary_chromosome=1011110011010011110111011011110000000110101001101011

python $sm_model_summary --model_path=$model_path  --summary_path=$summary_path --binary_chromosome=$binary_chromosome
```


TODO
----
1. combine two classes together
2. clean up the examples
"""

from argparse import ArgumentParser
import os, re
from functools import partial
import yaml
from termcolor import colored
import pandas as pd
import numpy as np
import copy
import logging
logging.getLogger('matplotlib').setLevel(logging.ERROR)
logging.getLogger("PIL.PngImagePlugin").setLevel(logging.ERROR) # PIL logs too much.
logging.getLogger("matplotlib.font_manager").setLevel(logging.ERROR) # matplotlib logs too much.

import matplotlib.pyplot as plt
import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout

class SMBaseModel(object):
    """SM Base Model Class
    Abstract base class of a class family for SM model.
    Primal example of the child calss is SMSegModel class. 
    This subclass supports the segmentation model using Cognitive AI.

    Parameters
    ----------
    model_path : str
        Path to a SM model file.
        Assume that every node exposed in the SM model is
        saved in the same directory that the model file is held.
    log : object
        logging class instance

    Attributes
    ----------
    self.model_path
        path to a SM model file
    self.log : object
        logging class instance
        
    Methods
    -------
    get_dict_nodes(self):
        Return a dictionary of nodes from the SM model network
        
    get_list_relationship(self):
        Return a list of relationships between nodes from the SM model network
    
    get_summary_graph(self, draw=True, figsize=(7,9), frameon=False, hover=False,
                    cmap='summer', node_size=500, font_size=8)
        Return a graph instance of the SM model.
        If you set draw=True, a matplotlob.pyplot.figure instance is returned together.

    Examples
    --------
    Python API examples
    ```
    import sm_model_summary

    model_path = f'/radraid/apps/personal/youngwon/debug_miu/cxr_model.9/et_tip_placement_model'
    
    log = logging.getLogger()
    formatter = logging.Formatter('[%(asctime)s|%(name)-10s|%(levelname)-8s|%(filename)-25s:%(lineno)-3s] %(message)s')
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    log.addHandler(ch)
    log.setLevel(logging.DEBUG)

    sm_model = SMBaseModel(model_path, log)
    print(sm_model.get_list_relationship())

    G, fig = sm_model.get_summary_graph(figsize=(5,10), font_size=12)
    fig.show()
    ```

    command-line usage
    ```
    python sm_model_summary.py --tool=graph
    ```

    """
    def __init__(self, model_path, log, verbose=2):
        """
        Parameters
        ----------
        model_path : str
            Path to a SM model file.
            Assume that every node exposed in the SM model is
            saved in the same directory that the model file is held.
        log : object
            logging class instance
        """
        self.model_path = model_path
        self.log = log
        self.log.info('---------------------------------------------------------------')
        self.log.info('SM model for segmentation')
        self.log.info('---------------------------------------------------------------')
        self.log.info(f'Model path: \n{model_path}')
        assert os.path.exists(self.model_path), f"[ERROR] Model path {model_path} is not available."
        self._read_nodes()
        self.log.info('---------------------------------------------------------------')
        self._get_relationships()
        self.log.info('---------------------------------------------------------------')
    
    def _read_nodes(self):
        with open(self.model_path) as f:
            nodes = f.readlines()
        start_num = [i for i in range(len(nodes)) if 'Model:' in nodes[i]][0]
        end_num = [i for i in range(len(nodes)) if 'End:' in nodes[i]][0]
        nodes = nodes[start_num+2:end_num]
        nodes = [node.strip() for node in nodes]
        nodes = nodes[::-1]
        self._nodes = ['input'] + nodes + ['output']
        self.log.info(f'Node list exposed from the model: \n{self._nodes}')
    
    def _get_relationships(self):
        self.log.info('Read relationship')
        self.log.info('---------------------------------------------------------------')
        list_relationship = []
        dict_nodes = {}
        model_base = os.path.dirname(self.model_path)
        dict_nodes['input'] = {'name': 'input', 'contents': 'input image', 'is_cnn':False,
                            'parents':None, 'value': 1}
        dict_nodes['output'] = {'name': 'output', 'contents': 'output result', 'is_cnn':False,
                                'parents':[self._nodes[-2]], 'value': 1}
        reverse_nodes = sorted(self._nodes, key=len, reverse=True)
        for child_node in self._nodes[1:-1]:
#             self.log.debug('---------------------------------------------------------------')
            self.log.info(f'Node: {child_node}')
            child_node_path = os.path.join(model_base, child_node)
            with open(child_node_path, 'r') as f:
                contents = ''.join(f.readlines()[1:]).split('End:')[0]

            parent_list = []
            nodes_except_current = [x for x in reverse_nodes if x is not child_node]
#             self.log.debug(nodes_except_current)
            contents_check = copy.copy(contents)
            for x in nodes_except_current:
#                 self.log.debug(f'check {x}')
                if np.all([x in contents_check,
                        f'{x}_' not in contents_check,
                        f'_{x}' not in contents_check]
                        ):
                    parent_list.append(x)
#                     self.log.debug(f'include {x} and replace')
                    contents_check = contents_check.replace(x, '')
#                     self.log.debug(f'\n{contents_check}\n')
            if not parent_list: 
                parent_list = ['input']
            self.log.info(f'\tparents: {parent_list}')
            dict_nodes[child_node] = {'name': child_node, 'contents': ''.join(contents),
                                    'is_cnn': ('_cnn' in child_node), 
                                    'parents':parent_list, 'value': (3 if '_cnn' in child_node else 2)}
        dict_nodes[self._nodes[1]]['parents'] = ['input']
        self._dict_nodes = dict_nodes
        
        list_relationship = []
        for child in self._nodes[1:]:
            list_relationship.extend([(parent, child) for parent in dict_nodes[child]['parents']])
#             self.log.debug(list_relationship)
        self._list_relationship = list_relationship
        self.log.info('---------------------------------------------------------------')
    
    def get_list_nodes(self):
        return self._nodes

    def get_list_relationship(self):
        return self._list_relationship
    
    def get_dict_nodes(self):
        return self._dict_nodes
    
    def get_summary_graph(self, draw=True, figsize=(7,9), frameon=False, hover=False,
                        cmap='summer', node_size=500, font_size=8):
        """get summary graph
        """
        self.log.info('Generating graph summary of the SM model')
        self.log.info('---------------------------------------------------------------')

        G = nx.DiGraph(directed=True)
        G.add_nodes_from(self._nodes)
        nx.set_node_attributes(G, self._dict_nodes)
        G.add_edges_from(self._list_relationship)
        
        if draw:
            fig = plt.figure(figsize=figsize, frameon=frameon)
            ax = fig.add_axes([0, 0, 1, 1])
            ax.axis('off')
            pos=graphviz_layout(G, prog='dot')
            node_values = [self._dict_nodes[node]['value'] for node in self._nodes]
            draw_nodes = nx.draw_networkx_nodes(G, pos, cmap=plt.get_cmap(cmap), 
                                   node_color = node_values, node_size = node_size, ax=ax)
            nx.draw_networkx_labels(G, pos, ax=ax, font_size=font_size,
                                    horizontalalignment='center',
                                    verticalalignment='center')
            nx.draw_networkx_edges(G, pos, arrows=True, ax=ax)
            annot_font_size = int(font_size*0.8)
            annot = ax.annotate("", xy=(0,0), xytext=(annot_font_size,annot_font_size),textcoords="offset points",
                                bbox=dict(boxstyle="round", fc="w"),
                                arrowprops=dict(arrowstyle="->"))
            annot.set_visible(False)

#             idx_to_node_dict = {}
#             for idx, node in enumerate(G.nodes):
#                 idx_to_node_dict[idx] = node

#             def _update_annot(self, ind):
#                 node_idx = ind["ind"][0]
#                 node = idx_to_node_dict[node_idx]

#             def _hover(self, event, fig, G):
#                 vis = annot.get_visible()
#                 if event.inaxes == ax:
#                     cont, ind = draw_nodes.contains(event)
#                     if cont:
#                         sm_model._update_annot(ind)
#                         annot.set_visible(True)
#                         fig.canvas.draw_idle()
#                     else:
#                         if vis:
#                             annot.set_visible(False)
#                             fig.canvas.draw_idle()
#             fig.canvas.mpl_connect("motion_notify_event", _hover)
            return G, fig
        else:
            return G


class ModelReader(object):
    """Text Summary Tool Class

    Parameters
    ----------
    model: str
        Path to a SM model file.
        Assume that every node exposed in the SM model is
        saved in the same directory that the model file is held.
    """

    def __init__(self, model):
        """
        Parameters
        ----------
        model: str
            Path to a SM model file.
            Assume that every node exposed in the SM model is
            saved in the same directory that the model file is held.
        """
        self.model_dir = os.path.dirname(model)
        print("================================================================================")
        print(f"Open model file: {model}")
        print("================================================================================")
        self.read_model(model)
        print("================================================================================")
        print(f"Finding nodes in model file from directory: {self.model_dir}")
        print("================================================================================")

        self.chr_bits = []
        for node in self.model_nodes:
            print("================================================================================")
            print(f"Finding {node}")
            self.find_chrom_bits(node)
        
        self.chr_bits = sorted(self.chr_bits, reverse=False, key=lambda t:t["ind_start"])
        
    def read_model(self, model):
        with open(model, 'r') as f:
            contents = f.read()

        self.model_nodes = []
        for l, line in enumerate(contents.split("\n")):
            if l==0:
                self.model_name = line.replace("Model: ", "").strip()
            else:
                if line[:4]=="End:":
                    break
                self.model_nodes.append(line.strip())
        print('Nodes in Model')
        print("================================================================================")
        print(f"{self.model_nodes}")
        print("================================================================================")

    def find_chrom_bits(self,node):
        node_file = os.path.join(self.model_dir, node)
        if not os.path.exists(node_file):
            print(f"No node file: {node_file}")
            return 
        print("==========================================================")
        print(f"Open node file: {node_file}")
        print("==========================================================")
        with open(node_file, 'r') as f:
            contents = f.read()
        node_id = None
        for l, line in enumerate(contents.split("\n")):
            if l==0:
                node_id = line.replace("AnatPathEntity: ", "").strip().strip(";")
                print(f"node id: {node_id}")
            else:
                if line[:4]=="End:":
                    print(f"Finish read {node_id}")
                    break
                node_line_id = f"{node_id}_{l}"
                for default_val, gene_group_id, bit_id, ind_start, ind_end, possible_values, bit_func, default_decimal_index, default_binary_index, colored_line in self.interpret_bit_line(line, node_line_id):
                    if default_val is None:
                        continue
                    print("DEBUG", node_id, bit_id)
                    self.chr_bits.append(dict(node_id=node_id, bit_id=bit_id, possible_values=possible_values,
                                            ind_start=ind_start, ind_end=ind_end, default_val=default_val,
                                            default_decimal_index=default_decimal_index, default_binary_index=default_binary_index,
                                            bit_func=bit_func, line=line, colored_line=colored_line))
    
    def interpret_bit_line(self, line, node_line_id):
        # w = re.compile(r"\s\S*\s\{[\S\s]*?\}")  
        w = re.compile(r"[0-9\-.ef]*\s\{[\S\s]*?\}") # searching for "default_val {ind_start, ind_end, val_start, val_end}""
        if w.search(line) is None:
            return None, None, None, None, None, None, None, None, None, None
        genes = re.findall(w, line)
        genes = [gene.strip() for gene in genes]
#         w = re.compile(r"[\w\s]+[\[|\-|\d]")
        w = re.compile(r"^([^\s]+)") #.+?(?=(\s\S*\s\{))
        gene_group_id = w.search(line).group().strip()
        print("----------------------------------------------------------")
        print(f"Detect genes from line: {line}")
        print("----------------------------------------------------------")
        print(f"genes: {genes}")
        print(f"gene_group_id: {gene_group_id}")
        print("----------------------------------------------------------")
        
        for i, gene in enumerate(genes):
            default_val, ind_start, ind_end, val_start, val_end = [x.strip() for x in re.split('[\{\},]', gene)[:-1]]
            default_val, val_start, val_end = float(default_val), float(val_start), float(val_end)
            ind_start, ind_end = int(ind_start), int(ind_end)
            bit_id = f"{node_line_id}_{gene_group_id}_{i}"
            start_num = line.find(gene)
            colored_line = line[:start_num] + colored(gene, 'red') + line[start_num+len(gene):]
            print(f"Parsing {i}th gene: {gene}")
            print(f"from line: {colored_line}")
            print(f"bit id: {bit_id}")
            print(f"default_val: {default_val}")
            print(f"ind_start: {ind_start}")
            print(f"ind_end: {ind_end}")
            print(f"val_start: {val_start}")
            print(f"val_end: {val_end}")
            bit_func = partial(self.bit_function, start_bit=ind_start, stop_bit=ind_end, low_value=val_start, high_value=val_end)
            possible_values = self.get_possible_values(ind_start, ind_end, val_start, val_end)
            default_decimal_index = round(self.reverse_bit_function(default_val, ind_start, ind_end, possible_values[0], possible_values[-1]))
            default_binary_index = str(format(default_decimal_index, '0%sb'%str(ind_end - ind_start+1)))[::-1]
            print(f"possible_values: {possible_values}")
            print(f"default_decimal_index: {default_decimal_index}")
            print(f"default_binary_index: {default_binary_index}")
            print("----------------------------------------------------------")
            yield default_val, gene_group_id, bit_id, ind_start, ind_end, possible_values, bit_func, default_decimal_index, default_binary_index, colored_line
            
    def get_possible_values(self, start_bit, stop_bit, low_value, high_value):
        gene_max_value = int(2.0**(stop_bit-start_bit+1) - 1)
        possible_values = [low_value + ((high_value-low_value)*gene_value/gene_max_value) for gene_value in range(gene_max_value+1)]
        return possible_values
    
    def reverse_bit_function(self, default_val, start_bit, stop_bit, low_value, high_value):
        default_val = float(default_val)
        gene_max_value = 2.0**(stop_bit-start_bit+1) - 1
        gene_value = ( default_val - low_value )* gene_max_value/(high_value-low_value)
        return gene_value

    def bit_function(self, binary_chr, start_bit, stop_bit, low_value, high_value):
        bit_section = binary_chr[start_bit:stop_bit+1]
        try:
            gene_value = int(bit_section,2)
            gene_max_value = 2.0**(stop_bit-start_bit+1) - 1
            v = low_value + ((high_value-low_value)*gene_value/gene_max_value)
            return v
        except:
            pass

    def output_parameters(self, binary_chr):
        for bit_dict in self.chr_bits:
            value = bit_dict["bit_func"](binary_chr)
            bit_dict["value"] = value
            yield bit_dict
                
    def get_default_chromosome(self):
        default_chromosome = ""
        colored_previous_chromosome = default_chromosome
        print("==============================================================================")
        print("start calculating default chromosome")
        df_chr_bits = pd.DataFrame(self.chr_bits)
        df_chr_bits = df_chr_bits[~df_chr_bits[['ind_start','ind_end','default_val']].duplicated()]
        df_chr_bits = df_chr_bits.sort_values(by="ind_start")
        ordered_chr_bits = df_chr_bits.to_dict("records")
        
        for bit_segment in ordered_chr_bits:
            print("==========================================================================")
            print("bit_id: ", bit_segment["bit_id"])
            decimal_index = bit_segment['default_decimal_index']
            binary_index = bit_segment['default_binary_index']
            default_chromosome+=binary_index
            print(f"decimal_index {decimal_index} --> binary_index {binary_index}")
            print("bit length:", bit_segment["ind_end"] - bit_segment["ind_start"]+1, "from", bit_segment["ind_start"], "to", bit_segment["ind_end"])
            print("default_val: ", bit_segment["default_val"], "out of", bit_segment["possible_values"])
            
            colored_previous_chromosome = default_chromosome[:-len(binary_index)]
            colored_addended_index = colored(default_chromosome[-len(binary_index):], "red")
            print("line: ", bit_segment["colored_line"])
            print(f"Appended default_chromosome: {colored_previous_chromosome}{colored_addended_index}")
        print("==========================================================================")
        print("Finished calculating default_chromosome")
        print("==========================================================================")
        print(f"Result default_chromosome: {default_chromosome}")
        print(f"Length of default_chromosome: {len(default_chromosome)}")
        print("==========================================================================")
        return default_chromosome







if __name__=='__main__':
    parser = ArgumentParser(description='MIU model summary')
    parser.add_argument('--tool', type=str, dest='tool', default='text_summary', 
                        help="two type of summary tools are supported: \
1) text_summary: summary tool for parsing model and getting default chromosome, \
2) graph_summary: summary tool for showing the node relationship in a graph view. The default is text_summary.")
    parser.add_argument('--model_path', type=str, dest='model_path', 
                        help="SM model to summarize")
    parser.add_argument('--summary_path', type=str, dest='summary_path', default='none', 
                        help="path to save text summary file from text summmary tool. if none, summry file will not be saved.")
    parser.add_argument('--binary_chromosome', type=str, dest='binary_chromosome', default='none',
                        help="binary_chromosome for text summary tool. if none, default chromosome will be used to summarize the model")
    args = parser.parse_args()
    
    if args.tool == 'graph_summary':
        pass
    elif args.tool == 'text_summary':
        model_reader = ModelReader(args.model_path)
        default_chromosome = model_reader.get_default_chromosome()

        if args.binary_chromosome.strip().lower() == "none":
            binary_chr = default_chromosome
        else:
            binary_chr = args.binary_chromosome.strip()

        if args.summary_path.strip() != "none":
            node_dict = dict()
            for params in model_reader.output_parameters(binary_chr):
                # print(params)
                # for params in model_reader.output_parameters(binary_chr):
                # k = ",".join((params["node_id"][:-1], params["bit_id"]))
                k = "_".join((params["node_id"], params["bit_id"]))
                # print(k)
                node_dict[k] = params
                
            with open(args.summary_path, 'w') as f:
                f.write(yaml.dump(dict(nodes=node_dict, node_list=model_reader.model_nodes, model=args.model_path)))
    else:
        raise ValueError(f'Tool name {args.tool} is not supported.')