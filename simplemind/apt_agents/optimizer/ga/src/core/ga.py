import os
import math
import pickle
import random

try:
    from deap import base
    from deap import creator
    from deap import tools
    from deap import algorithms
except:
    pass
import yaml
from collections import defaultdict
import time


import sys
from functools import partial
from simplemind.apt_agents.distributor.condor.src.qia.threadpool import ThreadPool
from simplemind.apt_agents.distributor.condor.src.qia.exceptions import ErrorValue
from simplemind.apt_agents.distributor.condor.src.qia.executecmd import execute_command
from simplemind.apt_agents.distributor.condor.src.pool import CondorPool, CondorPoolDAG
from simplemind.apt_agents.distributor.condor.src.docker import DockerPool
import numpy as np
from random import shuffle

from shutil import rmtree
import glob
from contextlib import nullcontext




##################################
# Reporting stuff
# Record the diversity before pruning (after the tournament selection)
# Record diversity after adding the diverse immigrants and computing the fitness


def Hamming_dist(ind1, ind2):
    diffs = 0
    for bit1, bit2 in zip(str(ind1), str(ind2)):
        if not bit1==bit2:
            diffs+=1
    return np.sqrt(diffs)

def compute_diversity(population, alpha=1):
    diversity = []
    _diversity = []
    n = len(population)
    for i, ind1 in enumerate(population):
        try:
            if ind1.fitness.values:
                fitness = ind1.fitness.values[0]
            else:
                fitness = ind1.fitness.parent_value[0]
        except:
            fitness = 1.0
        ind_div = 0
        for k, ind2 in enumerate(population):
            if i == k:
                continue
            dist = Hamming_dist(ind1, ind2)
            ind_div += dist
        div = ind_div/(n-1)
        f_norm_div = ind_div/(n-1) + alpha * fitness  # fitness normalized diversity
        # print(i, f_norm_div, fitness, binlist_to_hexstr(ind1))
        _diversity.append([f_norm_div, div, fitness, " ", binlist_to_hexstr(ind1)])
        diversity.append(f_norm_div)
    overall_diversity = np.mean(diversity)
    sorted_diversity = sorted(zip(diversity, population), reverse=True, key=lambda t: t[0])
    _diversity = sorted(_diversity, reverse=True, key=lambda t: t[2])
    for i, v in enumerate(sorted_diversity):
        d, ind = v
        for item in _diversity:
            if binlist_to_hexstr(ind)==item[4]:
                item[3]=i+1
                break

    return overall_diversity, sorted_diversity, _diversity



def record_diversity(pop, pruned_pop=None):
    overall_diversity, _, _diversity = compute_diversity(pop)
    # new_pop = diversity_enforced_pruning(gen["population"], 8.5, min_n_ind=5)
    if pruned_pop is not None:
        _new_pop = [binlist_to_hexstr(x) for x in pruned_pop]
        for x in _diversity:
            if x[-1] in _new_pop:
                x.append("")
            else:
                x.append("x")

    return {"overall_diversity": overall_diversity, "diversity_chart": _diversity}

######## For compact mode ##########

def delete_seg(chr_dir):
    if isinstance(chr_dir, list):
        chr_dir = chr_dir[0]
    cleaned_up_done = os.path.join(chr_dir, "cleaned_up.txt")
    if os.path.exists(cleaned_up_done) or not os.path.exists(chr_dir):
        return
    print(cleaned_up_done)
    # /scratch/wasil/data/node_cad/ga_2_experiment/island_4/3A1BA6EB5FC69295ADB84F4EB254B38A75D3187E2/LIDC-IDRI-0035/seg
    ## alternative ##
    
    # seg_dirs = [os.path.join(chr_dir, seg_dir, "seg") for seg_dir in os.listdir(chr_dir)]
    seg_dirs = [os.path.join(chr_dir, seg_dir, "seg") for seg_dir in glob.glob(os.path.join(chr_dir, "*",""))]
    print(seg_dirs[0])
    print(">",end="")
    
    for seg_dir in seg_dirs:
        if not os.path.exists(seg_dir):
            continue
        try:
            rmtree(seg_dir)
            print("*", end="")
        except:
            print("-", end="")
            pass
    with open(cleaned_up_done, 'w') as f:
        f.write("")


def clean(hof, population_history, name_input_ref_pair_lookup, local_workers=10):
    # if we have population history,
    #   iterate through each chromosome
    #   translate into hex
    #   append to base_dir
    #   check to see if "cleaned_up.txt" exists
    #   if not, then delete the seg file (shutil)
    #   
    hof_hex = [binlist_to_hexstr(chrom) for chrom in hof]
    # print("Hall of Fame:")
    # [print(chrom) for chrom in hof_hex]
    if population_history is not None:
        delete_dirs = []
        for chrom in population_history:
            chr_hex = binlist_to_hexstr(chrom)
            # print(chr_hex)
            if not chr_hex in hof_hex:
                print(".", end="")
                # print("Cleaning directory...")
                for _, case in name_input_ref_pair_lookup.items():
                    # delete_seg(os.path.join(case[0]["results_dir"], chr_hex))
                    delete_dirs.append(os.path.join(case[0]["results_dir"], chr_hex))
        delete_dirs = list(set(delete_dirs))
        parallel_args = [ [ x, ] for x in delete_dirs ]
        print("Cleaning up in parallel:")
        print("Parallel Workers:", local_workers)
        map_result = parallelize(delete_seg, parallel_args, parallel_scheme="threading", custom_pool_size=local_workers) 
        [x for x in map_result]    # to execute if need be
        # no error catching here because it's more efficient to just clean it the next time around
    return 

###############################



def restricted_tournament(population, n_worst, hfc_tournsize, survivor_pop, tourn_func=None):
    if tourn_func is None:
        tourn_func = tools.selTournament
    sorted_inds = sorted(population, reverse=False, key=lambda t:float(t.fitness.values[0]))
    print("Worst", "Last")
    print(sorted_inds[0].fitness.values, sorted_inds[-1].fitness.values)
    r_tn_inds = sorted_inds[:n_worst]
    return tourn_func(r_tn_inds, k=survivor_pop, tournsize=hfc_tournsize)

def tournament(population, tourn_func, hfc_tourn_func=None):
    overall_pop = tourn_func(population)
    record = {"overall_tourn":{
                                "before": population,
                                "after": list(overall_pop),
            }}
    if hfc_tourn_func is not None:
        hfc_pop = hfc_tourn_func(population)
        # [print(x.fitness.values, x.id) for x in hfc_pop]
        # print("HFC")
        overall_pop.extend(hfc_pop)
        #record
        record["hfc_tourn"] = {   "before": population,
                                    "after": hfc_pop,
                                    }

    record["tourn"] = {     "before": population,
                            "after": overall_pop,
                            "diversity": compute_diversity(population, alpha=0) if len(population) > 1 else 0
                                }
    # [print(x.fitness.values, x.id) for x in overall_pop]
    return overall_pop, record

def mutate(population, tb, mutpb):
    mutated_population = []
    mutated_indices = []
    for i, ind in enumerate(population):
        parent_value = ind.fitness.values
        ind.fitness.parent_value = parent_value
        id = ind.id
        if random.random() + mutpb > 1:
            mut_ind = tb.mutate(creator.Individual(ind))[0]
            mut_ind.fitness.parent_value = parent_value
            mut_ind.id = id
            if list(mut_ind)==list(ind):
                # print("the same.")
                mut_ind.fitness.values = parent_value
            else:
                mutated_indices.append(i)
            mutated_population.append(mut_ind)
            # print("".join([str(x) for x in ind]))
            # print("".join([str(x) for x in mut_ind]))
            # print("".join([str(x) for x in copy]))
            #
            # print(mut_ind.fitness.values)
            # print(mut_ind.fitness.parent_value)
            # print("After^^")
            # input()
    record = {"mutation":  {"before": population,
                           "after": mutated_population,
                           "mutated_indices": mutated_indices,
                           } }
    return mutated_population, record

# parent_value[0] will always have the initial value before going through mutate operator
# fitness.values[0] will just be the initial value for now

def crossover(population, cx_op, cxpb, cx_limit=1):
    # print(">>>>>")
    # [print(x.id) for x in population]
    #for recording purposes:
    orig_pop = population
    crossed_over_indices = []
    cx_indices = {i:0 for i in range(len(population))}

    for i in range(len(population)):
        # print(i)
        j = i+1
        if j == len(population):
            j = 0
        if random.random() + cxpb > 1:
            if cx_indices[i]+1 > cx_limit or cx_indices[j]+1 > cx_limit:
                continue
            cx_indices[i] += 1
            cx_indices[j] += 1
            print(cx_indices[i], cx_indices[j])
            ind1 = orig_pop[i]  #originally taken from "population" but want to avoid chain crossings in one generation
            ind2 = orig_pop[j]  #originally taken from "population" but want to avoid chain crossings in one generation
            # print(ind1.fitness.parent_value[0], ind2.fitness.parent_value[0])
            # print(ind1.id, ind2.id)
            combined_ids = list(ind1.id)
            combined_ids.extend(ind2.id)
            # print(combined_ids)
            combined_ids = list(set(combined_ids))
            # print(combined_ids)
            # print(ind1.id.extend(ind2.id))
            # print(set(ind1.id.extend(ind2.id)))
            # combined_ids = list(set(ind1.id.extend(ind2.id)))
            # combined_ids = list( set(ind1.id).update(ind2.id))
            averaged_fitness = (ind1.fitness.parent_value[0] + ind2.fitness.parent_value[0] )/2
            ind1, ind2 = cx_op(creator.Individual(ind1), creator.Individual(ind2))
            ind1.fitness.parent_value = ind2.fitness.parent_value = (averaged_fitness, )
            ind1.id = ind2.id = combined_ids
            population[i] = ind1
            population[j] = ind2
            crossed_over_indices.extend([i, j])
            # print(ind1.fitness.values, ind2.fitness.values)
            # print(ind1.fitness.parent_value, ind2.fitness.parent_value)
            # input()
    record = {"crossover":  {"before": orig_pop,
                           "after": population,
                           "crossed_over_indices": set(crossed_over_indices),
                           } }

    return population, record



def diversity_enforced_pruning(population, min_diversity, min_n_ind=5, alpha=1):
    overall_diversity, diversity, _ = compute_diversity(population, alpha=alpha)
    n_ind = len(diversity)
    # [print(x) for x in diversity]
    # [print(x) for x in _]
    print("Minimum diversity:", min_diversity)
    print(overall_diversity, "(initial)")
    while overall_diversity <= min_diversity and n_ind > min_n_ind:
        overall_diversity, diversity, _ = compute_diversity(population[:n_ind-1], alpha=alpha)
        print(">",overall_diversity)
        n_ind -=1
    return [ind for _, ind in diversity]


def gen_ind(bit_dist):
    ind_shape = bit_dist.shape
    # print(bit_dist)
    # print("Seed",np.random.get_state())
    ind = np.round(np.random.rand(ind_shape[0]) + (-1 * (bit_dist.astype('float64') - .5))).astype("int8")
    return ind

def _homogenize_length(population_history):
    max_len = np.max([len(ind) for ind in population_history])
    for ind in population_history:
        while len(ind) < max_len:
            ind.append(0)
    return population_history

### just a wrapper for default random immigrants
def default_immigrants(n_immigrants, default_pop_func, id_counter):    
    immigrants = default_pop_func()[:n_immigrants]
    for immigrant in immigrants:
        id_counter+=1
        immigrant.id = [id_counter, ]
    return immigrants
    
def diversified_immigrants(n_immigrants, population_history, id_counter):
    immigrants = []
    population_history = _homogenize_length(population_history)
    print("Population history", len(population_history))
    print("Id Counter:", id_counter)
    all_inds = np.array([np.array([int(i) for i in ind]) for ind in population_history])
    n_inds = all_inds.shape[0]
    inds_sum = np.sum(all_inds, axis=0)

    for i in range(n_immigrants):
        inds_mean = inds_sum / n_inds
        population_match = True
        while population_match:
            imm_npy = gen_ind(inds_mean)
            bin_str = [int(i) for i in imm_npy]
            diverse_immigrant = creator.Individual(bin_str)
            population_match = diverse_immigrant in population_history
        inds_sum += imm_npy
        n_inds += 1
        id_counter+=1
        diverse_immigrant.id = [id_counter, ]
        immigrants.append(diverse_immigrant)
        population_history.append(diverse_immigrant)

    return immigrants

def repop(population, n_pop, repop_func):
    orig_pop = list(population)
    n_immigrants = n_pop - len(population)
    immigrants = repop_func(n_immigrants)


    # print("n_immigrants:", n_immigrants)
    # print("before:", population)
    population.extend(immigrants)
    # print("after:", population)
    record = {"immigrants":{
                                "before": orig_pop,
                                "after": population,
                                "diversity": compute_diversity(population),
                                "n_immigrants":n_immigrants,
            }}

    # print("after immigrants")
    # input(record)
    return population, record

# (A)
def dep(population, dep_func):
    reduced_population = dep_func(population)

    record = {"dep":{
                                "before": population,
                                "after": list(reduced_population),
                                "chart": record_diversity(population, pruned_pop=reduced_population),
            }}

    # population, record_fill_pop  = fill_pop(reduced_population, len(population), repop_func,)
    # n_immigrants = len(population) - len(reduced_population)
    # record.update(record_fill_pop)
    return reduced_population, record

    # immigrants = repop_func(n_immigrants)[:n_immigrants]    # takes the last n_immigrants in case repop_func is the default random population() func that just generates n_population number
    # reduced_population.extend(immigrants)
    # # overall_diversity, _, _diversity = compute_diversity(reduced_population)
    # record = {"immigrants":{
    #                             "before": population,
    #                             "after": reduced_population,
    #                             "diversity": compute_diversity(population),
    #                             "n_immigrants":n_immigrants,
    #         }}

    # return reduced_population, record


## Perform GA operations ##
# 1. Overall tournament
# 2. HFC Tournament
# 3. Mutation + Crossover
# 4. Diversity-enforced pruning
# 5. Diversified Immigrants
# 6. Fitness evaluation

## Class for taking care of evolving generation by generation 
# TODO: Document how to implement new evolutionary operator
# (A) Make the function
#       - return population, record
# (B) Add to self.log
class Evolver():
    #update ga_param to have population_history
    def __init__(self, toolbox, ga_param=None, record=False, log=None):
        self.tb = toolbox
        self.di = False
        self.record = record
        self.op_names = []
        #default, if nothing is activated:
        if ga_param is None:
            n_worst = 0
            ga_param = { "hfc": False,
                        "dep": False,
                        "di": False,
                            }

            # default tournament
            p_mut_per_bit = .01
            overall_t_size = 10
            tournsize = 3
            self.tb.register("mate", tools.cxTwoPoint)
            self.tb.register("mutate", tools.mutFlipBit, indpb=p_mut_per_bit)
            self.tb.register("overall_tourn", tools.selTournament, tournsize=tournsize, k=overall_t_size)
            self.ga_param = {}
        else:
            self.ga_param = ga_param
            self.tb_ops = []
            self.register_operations(ga_param["operations"])

        if self.record:
            # self.overall_tourn_record = {"before":[], "after":[]}
            # self.hfc_tourn_record = {"before":[], "after":[]}
            # self.mutation_record = {"before":[], "after":[]}
            # self.crossover_record = {"before":[], "after":[]}
            if log is not None:
                self.log = log
            else:
                ### (B)
                self.log = {
                    "tourn": [],
                    "overall_tourn": [],
                    "hfc_tourn": [],
                    "mutation": [],
                    "crossover": [],
                    "dep": [],
                    "immigrants": [],
                }

    def evolve(self, population):
        # print("repop",self.tb.repop)
        if len(population) > 1:
            overall_diversity, _, _ = compute_diversity(population, alpha=1)
            print("Before evolution:",overall_diversity)
        [print(x.fitness.values, x.id) for x in population]
        for i, op in enumerate(self.tb_ops):
            print(self.op_names[i])
            output = op(population)
            if len(output) == 2:
                population, record = output
                self.log_activity(record)
            else:
                population = output
                self.log_activity({self.op_names[i]: population})
            if len(population) > 1:
                overall_diversity, _, _ = compute_diversity(population, alpha=1)
                print("After op:",overall_diversity)
            [print(x.fitness.values, x.id) for x in population]
            # print("After op:",population[0].fitness.values)
            # [print("".join([str(i) for i in x])) for x in population]
        self.tb.population_history.extend(population)
        return population

    #record comes in format of {"key": info, "key2":info2}
    def log_activity(self, record):
        if record.get("immigrants") is not None:
            # print("Updating counter from immigrants")
            self.tb.id_counter+=record["immigrants"]["n_immigrants"]
            # input(self.tb.id_counter)  #REACTIVATE
        if self.record:
            for key, info in record.items():
                if self.log.get(key) is not None:
                    self.log[key].append(info)

    def register_operations(self, operations):
        # - "overall_tourn"
        # - "hfc_tourn"
        # - "mutation"
        # - "crossover"
        # - "dep"
        # - "repop"
        
        ### (C)
        for op in operations:
            if op == "tourn":
                hfc_params = self.ga_param.get("hfc")
                self.add_tourn(self.ga_param["overall_t_size"], self.ga_param["overall_survivor_pop"], hfc_params=hfc_params)
                self.op_names.append(op)
            elif op == "shuffle":
                self.add_shuffle()
                self.op_names.append(op)
            elif op == "mutation": 
                self.add_mut(self.ga_param["p_mut_per_bit"], self.ga_param["mutpb"])
                self.op_names.append(op)
            elif op == "crossover":
                self.add_cx(self.ga_param["cxpb"],)
                self.op_names.append(op)
            elif op == "dep":
                self.add_dep(self.ga_param["dep"]["min_diversity"],
                            min_n_ind=self.ga_param["dep"]["min_n_ind"],
                            alpha=self.ga_param["dep"]["alpha"],
                            # di = self.ga_param["di"],
                            # population_history = self.tb.population_history,
                            # id_counter = self.tb.id_counter,

                             )
                self.op_names.append(op)
            elif op == "repop":
                self.add_repop(di=self.ga_param["di"],
                            population_history=self.tb.population_history,
                            id_counter=self.tb.id_counter,
                             )
                self.op_names.append(op)

    def add_tourn(self, overall_t_size, overall_survivor_pop, hfc_params=None):
        self.tb.register("overall_tourn", tools.selTournament, tournsize=overall_t_size, k=overall_survivor_pop)
        if hfc_params is not None:
            self.tb.register("restricted_tourn", restricted_tournament, n_worst=hfc_params["n_worst"], hfc_tournsize=hfc_params["t_size"],
                             survivor_pop=hfc_params["survivor_pop"], tourn_func=tools.selTournament)
        else:
            self.tb.restricted_tourn = None     #dirty
        self.tb.register("tournament", tournament, tourn_func=self.tb.overall_tourn, hfc_tourn_func=self.tb.restricted_tourn)
        self.tb_ops.append(partial(self.tb.tournament))

    def add_shuffle(self):
        self.tb_ops.append(partial(sorted, key=lambda k: random.random()))

    def add_mut(self, p_mut_per_bit, mutpb):
        self.tb.register("mutate", tools.mutFlipBit, indpb=p_mut_per_bit)
        mut = partial(mutate, tb=self.tb, mutpb=mutpb)
        self.tb_ops.append(mut)

    def add_cx(self, cxpb):
        # ORDER MATTERS, SO NEED TO SHUFFLE BEFORE PUTTING IN HERE
        self.tb.register("mate", tools.cxTwoPoint)
        cx = partial(crossover, cx_op=self.tb.mate, cxpb=cxpb)
        self.tb_ops.append(cx)

    def add_mut_cxr(self, p_mut_per_bit, cxpb, mutpb):
        # ORDER MATTERS, SO NEED TO SHUFFLE BEFORE PUTTING IN HERE
        self.tb.register("mate", tools.cxTwoPoint)
        self.tb.register("mutate", tools.mutFlipBit, indpb=p_mut_per_bit)
        mut_cxr = partial(algorithms.varAnd, toolbox=self.tb, cxpb=cxpb, mutpb=mutpb)
        self.tb_ops.append(mut_cxr)

    ### (D)
    def add_dep(self, min_diversity, min_n_ind=5, alpha=0):
        self.tb.register("dep", diversity_enforced_pruning, min_diversity=min_diversity, min_n_ind=min_n_ind, alpha=alpha)
        # if di:
        #     self.di = True
        #     self.tb.register("repop", diversified_immigrants, population_history=population_history, id_counter=id_counter)
        # else:
        #     self.di = False
        #     self.tb.register("repop", self.tb.population(), id_counter=id_counter)
        self.tb.register("dep", dep, dep_func=self.tb.dep)
        self.tb_ops.append(partial(self.tb.dep))

    def add_repop(self, di=False, population_history=None, id_counter=-1):
        if di:
            self.di = True
            self.tb.register("repop_method", diversified_immigrants, population_history=population_history, id_counter=id_counter)
        else:
            self.di = False
            self.tb.register("repop_method", default_immigrants, default_pop_func=self.tb.population, id_counter=id_counter)
        self.tb.register("repop", repop, n_pop=self.ga_param["overall_pop_size"], repop_func=self.tb.repop_method)
        self.tb_ops.append(partial(self.tb.repop))


    def update_tb(self, population_history=None, id_counter=None):
        if population_history is not None:
            self.tb.population_history = population_history
        if id_counter is not None:
            self.tb.id_counter = id_counter

        print("Updated TB id counter", id_counter)
        print("Updated TB id counter", self.tb.id_counter)

        # try:
        #     dep_ind = self.op_names.index("dep")
        # except:
        #     dep_ind = None
        # if dep_ind is not None:
        #     self.tb.unregister("dep_repop")
        #     self.tb.unregister("repop")
        #     if self.di:
        #         self.tb.register("repop", diversified_immigrants, population_history=self.tb.population_history, id_counter=int(self.tb.id_counter))
        #     else:
        #         #update also for default generation
        #         self.tb.register("repop", population_history=self.tb.population_history, id_counter=int(self.tb.id_counter))
        #     self.tb.register("dep_repop", dep_repop, dep_func=self.tb.dep, repop_func=self.tb.repop)
        #     self.tb_ops[dep_ind] = self.tb.dep_repop

    def clean(self, halloffame):
        self.tb.clean(halloffame, self.tb.population_history)

try:    #formerly to make it condor compatible
    class ImprovedToolbox(base.Toolbox):
        def __init__(self):
            base.Toolbox.__init__(self)
            self.id_counter = -1
            self.population_history = []
        def get_id(self):
            self.id_counter+=1
            return [self.id_counter, ]
        def add_ind(self, ind):
            self.population_history.append(ind)
            return
except:
    pass
def new_toolbox(creator_individual, eval_pop_func, ga_param, cleaner):
    """Helper function that returns a deap.base.Toolbox object.
    :param creator_individual: The type of the individual created via deap.creator.create function. Below is an example:
        from deap import creator
        creator.create("FitnessMax", base.Fitness, weights=(1.0,))
        creator.create("Individual", list, fitness=creator.FitnessMax)
    :param eval_pop_func: A function to evaluate a given population.
    :param vec_size: Number of bits in a single individual.
    :param pop_size: Number of individual in a population.
    :param t_size: Tournament size (number of selected individuals per selection).
    :param p_mut_per_bit: Probability of mutation at the bit level.
    """
    toolbox = ImprovedToolbox()
    toolbox.register("attr_bit", random.randint, 0, 1)
    toolbox.register("individual", tools.initRepeat, creator_individual, toolbox.attr_bit, ga_param["vec_size"])
    toolbox.register("population", tools.initRepeat, list, toolbox.individual, ga_param["overall_pop_size"])
    toolbox.register("evaluate", eval_pop_func)
    toolbox.register("clean", cleaner) 
        
    return toolbox

def evolve(ga_param, max_gen, toolbox, stats=None, checkpoint=None, seed=None, halloffame=None, genes=(), evolve_genes=False, gene_hex=False):
    """Starts the evolution process.
    
    :param max_gen: Maximum number of generation to evolve.
    :param cxpb: Crossover probability.
    :param mutpb: Mutation probability.
    :param toolbox:  A deap.base.Toolbox object, the following attributes is expected in toolbox:
        population(): Create a population for evaluation.
        evaluate(population): Evaluates a given population, expects fitness of individuals to be set within the function.
        mate(child1, child2): Apply crossover operator on two children.
        mutate(individual): Apply mutation operator on an individual.
        select(population): Selects individual for next generation.
    :param stats: A deap.tools.Statistics object
    :param checkpoint: A string containing the file for checkpoint retrieval and storage.
    :param seed: The randomize seed
    :param halloffame: A deap.tools.HallOfFame object. Will create one internally for checkpointing if not provided.
    
    Returns a tuple containing the population from the last generation and a deap.tools.Logbook object.
    """
    evolved_gen = False
    surviving_population = {}
    offspring_population = {}
    if not evolve_genes:
        # One time run only, on specified genes
        # if genes:
        if not genes:
            raise("No genes supplied.")
        creator.create("FitnessMax", base.Fitness, weights=(1.0,))
        creator.create("Individual", list, fitness=creator.FitnessMax)
        population = toolbox.population()
        for i, gene in enumerate(genes):
            bin_str = gene
            if gene is not None:
                print("Doing gene", gene)
                if gene_hex:
                    #hex strings are saved backwards for readability
                    bin_str = str(bin(eval("0xF"+gene[::-1]))).replace("0b1111","")[::-1]
                population[i] = creator.Individual(bin_str,)
            else:
                population[i] = bin_str

        toolbox.evaluate(population[:(i+1)])
        return population, None
    elif checkpoint is not None and os.path.exists(checkpoint):
        print("Loading checkpoint")
        with open(checkpoint, "rb") as f:
            cp = pickle.load(f)
        population = cp["population"]
        start_gen = cp["generation"]
        halloffame = cp["halloffame"]
        logbook = cp["logbook"]
        evolved_gen = cp.get("evolved_gen", False)
        offspring_population = cp.get("offspring_population", {}) # start of a new generation
        toolbox.population_history = cp.get("population_history", population)
        previous_log = cp.get("log", None)
        toolbox.id_counter = cp.get("id_counter", -1)
        if toolbox.canary is not None:
            print("Canary params :::: ",cp.get("canary_params"))
            toolbox.canary.update_params(cp.get("canary_params", {}))

        random.setstate(cp["rndstate"])
    else:
        print("Initializing new GA run")
        print("seed:", seed)
        if seed is not None:
            random.seed(seed)
            np.random.seed(seed)
        population = toolbox.population()
        print("Initialized population size:", len(population))
        for i, gene in enumerate(genes):
            if gene_hex:
                #hex strings are saved backwards for readability
                bin_str = str(bin(eval("0xF"+gene[::-1]))).replace("0b1111","")[::-1]
            else:
                bin_str = str(gene)
            bin_str = [int(i) for i in bin_str]
            population[i] = creator.Individual(bin_str,)  #can work
        print("Added chromosomes:", len(genes))
        print("After filling population with chrs:", len(population))
        for ind in population:  #attach an id to each individual
            ind.id = toolbox.get_id()
        start_gen = 0
        if halloffame is None:
            halloffame = tools.HallOfFame(10)
        logbook = tools.Logbook()
        offspring_population = {0: population}
        toolbox.population_history = list(population)    #fixes onee problem but introduces another?
        previous_log = None

    evolver = Evolver(toolbox, ga_param=ga_param, record=True, log=previous_log)
    for gen in range(start_gen, max_gen):
        print("================= Starting new generation ================")
        print("Generation:", gen)
        if gen!=0 and not evolved_gen:
            population = evolver.evolve(population)
            if toolbox.canary is not None: toolbox.canary.update_best_fitness(halloffame)   # what happens if initiating
            print("after evolution id counter:", evolver.tb.id_counter)
            evolver.update_tb()
            # toolbox.id_counter = evolver.tb.id_counter
            # print("after evolution outside toolbox id counter:", toolbox.id_counter)
            # toolbox.population_history.extend(population)
            # evolver.update_tb(population_history=toolbox.population_history)
            # evolver.update_tb(population=toolbox.population_history)
            evolved_gen = True

        print("Pre-Saving checkpoint...")
        if checkpoint is not None:
            if not os.path.exists(os.path.dirname(checkpoint)):
                os.makedirs(os.path.dirname(checkpoint), exist_ok=True)
            if evolver.tb.canary is not None: canary_params=evolver.tb.canary.export_params()
            else: canary_params=None
            cp = dict(
                population=population, generation=gen, halloffame=halloffame,
                offspring_population=offspring_population,
                logbook=logbook, rndstate=random.getstate(), evolved_gen=evolved_gen,
                population_history=evolver.tb.population_history, log=evolver.log, id_counter=evolver.tb.id_counter,
                canary_params=canary_params,
            )
            with open(checkpoint, "wb") as cp_file:
                pickle.dump(cp, cp_file)

        invalid_ind = [ind for ind in population if not ind.fitness.valid]
        toolbox.evaluate(invalid_ind, canary_obj=toolbox.canary)    #unsure if this is better to be evolver.tb.evaluate

        halloffame.update(population)
        offspring_population[gen] = population  #unsure if I need this

        print("Begin cleaning..")
        evolver.clean(halloffame)   # if compact was not specified then cleaning is just a dummy function


        ## not tested ##
        if stats is not None:
            record = stats.compile(population)
            logbook.record(gen=gen, evals=len(invalid_ind), population=population, **record)
        else:
            logbook.record(gen=gen, evals=len(invalid_ind), population=population)
        #################

        print("Saving checkpoint...")
        evolved_gen = False
        if checkpoint is not None:
            if not os.path.exists(os.path.dirname(checkpoint)):
                os.makedirs(os.path.dirname(checkpoint), exist_ok=True)
            if evolver.tb.canary is not None: canary_params=evolver.tb.canary.export_params()
            else: canary_params=None
            cp = dict(
                population=population, generation=gen, halloffame=halloffame,
                offspring_population=offspring_population,
                logbook=logbook, rndstate=random.getstate(), evolved_gen=evolved_gen,
                population_history=evolver.tb.population_history, log=evolver.log, id_counter=evolver.tb.id_counter,
                canary_params=canary_params,
            )
            with open(checkpoint, "wb") as cp_file:
                pickle.dump(cp, cp_file)

    return population, logbook
    
## Originally: 
#   return (('%%0%sX' % math.ceil(len(str(input))/4)) % int("".join((str(i) for i in reversed(str(input)))),2))[::-1]
def binlist_to_hexstr(bin_input):
    """Returns a hexdecimal string from a given list of 0s and 1s.
    Below is an example:
        Command: binlist_to_hexstr([1,1,0,0,1,0,1,1,0])
        Expected ouput: 3D0
    """
    if bin_input is None:
        print("Warning: Conversion binary was None.")
        return None
    bin_input = "".join((str(i) for i in reversed(bin_input)))
    return (('%%0%sX' % math.ceil(len(str(bin_input))/4)) % int(bin_input,2))[::-1]

def _compute_evaluation(param):
    overall_start = time.time()

    gene_str, input_param, reference, output, segment_func, eval_func, results_loader, skip, reset = param
    if skip is None: skip = dict()
    """Meant to be used internally by evaluate_population.
    Below are the contracts for segment_func and eval_func:
       segment_func(gene_str, input, segment_outpath): Expect an output that eval_func can use.
       eval_func(segment_func_return_value, reference, eval_outpath): Expect the evaluation results.
    Both functions are expected to raise exception in case of errors.
    """
    overall_start = time.time()
    results = results_loader(output)
    if reset:
        reset_files = [output["seg_res"], output["seg_done"], output["seg_img"], ]
        for reset_file in reset_files:
            if os.path.exists(reset_file): 
                os.remove(reset_file)
        seg_out = eval_out = None

    try:
        if not os.path.exists(output["seg"]):
            os.makedirs(output["seg"], exist_ok=True) 
    except:
        pass

    seg_out = results.seg_out()
    if seg_out is None:
        starttime = time.time()
        input_param["rerun"] = True
        seg_out = segment_func(gene_str, input_param, output["seg"], log_file=output["log_file"])

        if not results.seg_finished():
            print("Warning: Segmentation Failed During Initial Attempt... Waiting 15s before retrying...")
            time.sleep(15)
            seg_out = segment_func(gene_str, input_param, output["seg"], log_file=output["log_file"])

        if not results.seg_finished():
            raise("Segmentation unfinished. Aborting...")

        with open(output["seg_res"], "w") as f:
            f.write(yaml.dump(seg_out))
        time_elapsed = time.time() - starttime
        print(output["seg_done"])
        print("CAD Runtime:", time_elapsed)

    eval_out = results.eval_out()
    if eval_out is None:
        starttime = time.time()
        print("Running eval...", output["eval_file"])
        try:
            eval_out = eval_func(seg_out, reference, output["eval"], dataset=input_param["dataset"], id=input_param["id"], skip_images=skip.get("eval_screenshots", False)) ## TODO: change this to skip later
            with open(output["eval_file"], "w") as f:
                f.write(yaml.dump(eval_out))
        except Exception as e: 
            print("Exception in running eval", e)
            print("Running seg again:")
            input_param["rerun"] = True
            seg_out = segment_func(gene_str, input_param, output["seg"], log_file=output["log_file"])
            starttime = time.time()
            eval_out = eval_func(seg_out, reference, output["eval"], dataset=input_param["dataset"], id=input_param["id"], skip_images=skip.get("eval_screenshots", False))
            with open(output["eval_file"], "w") as f:
                f.write(yaml.dump(eval_out))

        time_elapsed = time.time() - starttime
        print(output["eval_file"])
        print("Eval Runtime:", time_elapsed)


    time_elapsed = time.time() - overall_start
    print(output["eval_file"])
    print("Overall Processing:", time_elapsed)
    eval_out["finished"] = True
    eval_out["gene_str"] = gene_str

    return eval_out
    
# helper function for evaluation loading parallelization
def _get_evaluation_result(params):
    results_loader, output = params
    results = results_loader(output)
    return results.eval_out()

def docker_return_func(params):
    exec_cmd, _params = params
    execute_command(exec_cmd.split(" "))
    gene_str, input_param, reference, output, segment_func, eval_func, results_loader, skip_images, reset = _params
    return _get_evaluation_result((results_loader, output)) 

def _compile(params):
    compile_func, _eval_results, _path, rerun_compile, canary = params
    # fitness_output = compile_func(eval_results[gene_str], os.path.join(chrom_props[chrom_ind]["dataset_info"][dataset_index]["result_root"], gene_str), rerun=rerun_compile, canary=canary)
    fitness_output = compile_func(_eval_results, _path, rerun=rerun_compile, canary=canary)
    return fitness_output
class MapFunc():
    def __init__(self, pool=None, pool2=None, map=None, map_close=None, map_join=None, map_is_alive=None):
        self.pool = pool
        if map is not None:
            self._map = map
        elif pool is not None:
            self._map = pool.map
        else:
            raise("Map not defined.")
        
        if map_close is not None:
            self._map_close = map_close
        elif pool is not None:
            self._map_close = pool.close
        else:
            self._map_close = self.return_none
        
        if map_join is not None:
            self._map_join = map_join
        elif pool is not None:
            self._map_join = pool.join
        else:
            self._map_join = self.return_none        
        
        if map_is_alive is not None:
            self._map_is_alive = map_is_alive
        else:
            self._map_is_alive = self.return_none
                
    def return_none(self):
        return None
    def map(self, *args):
        return self._map(*args)
        
    def close(self):
        return self._map_close()
        
    def join(self):
        return self._map_join()

    def is_alive(self):
        return self._map_is_alive()
        
## Options: 
## - condor
## - docker
## - threading
## - regular map
# these might all be the same but for now just making it work
def get_map_func(parallel_scheme=None, pooling_args=None):
    if pooling_args is None:
        pooling_args = {}
        
    pool = pooling_args.get("pool")
    if parallel_scheme in ("condor", "docker"):
        pool.set_timeout(pooling_args.get("timeout"))
        if parallel_scheme=="docker" and pooling_args.get("return_func") is not None:
            pool.set_return_func(pooling_args["return_func"])
        poolmap = partial(pool.map, workpath_generator=pooling_args.get("workpath_generator"), creator=pooling_args.get("condor_creator"), condor_venv=pooling_args.get("condor_venv", ""), os_env=pooling_args.get("os_env", "ldap"), prepcode=pooling_args.get("prepcode"))
        map_func = MapFunc( map=poolmap,
                            map_join=pool.join,
                            map_close=pool.close,
                            map_is_alive = pool._pool._pool_join_thread.is_alive if parallel_scheme=="condor" else None,
                            )
    # elif parallel_scheme=="docker":
    #     poolmap = partial(pool.map, workpath_generator=pooling_args.get("workpath_generator"), creator=pooling_args.get("condor_creator"), condor_venv=pooling_args.get("condor_venv", ""), os_env=pooling_args.get("os_env", "ldap"), prepcode=pooling_args.get("prepcode"))
    #     map_func = MapFunc( map=poolmap,
    #                         map_join=pool.join,
    #                         map_close=pool.close,                            
    #                         )

    elif parallel_scheme=="threading":
        map_func = MapFunc( map=pool.map,
                            map_join=pool.join,
                            map_close=pool.close,
                            # map2=map,
                            
                            )
    else:
        map_func = MapFunc(map=map)
    return map_func

## pooling args can only be None if parallel scheme is threading
## if parallel_scheme is CondorPoolDAG, then must be list of dictionaries
##                                          where each dictionary is: 
##                                              chrom["case_infos"] = [dict("args"=[arg1,arg2]),]
## if otherwise [ [arg1,arg2], [arg1,arg2], [arg1,arg2], [arg1,arg2], ]
def parallelize(parallel_func, arg_list, parallel_scheme="", pooling_args=None, custom_pool_size=None):
    if pooling_args is None:
        pooling_args = dict()
    
    pool_size =  len(arg_list) if len(arg_list) < pooling_args.get("parallel_sub_dag_n", 20) else pooling_args.get("parallel_sub_dag_n", 20)
    # input()
    if parallel_scheme=="condor":
        pool_size =  len(arg_list[0]) if len(arg_list[0]) < pooling_args.get("parallel_dag_n", 20) else pooling_args.get("parallel_dag_n", 20)
        pool_func = CondorPoolDAG
        aggregate = False
    elif parallel_scheme=="docker":
        pool_func = DockerPool
    else:
        pool_func = ThreadPool

    if custom_pool_size is not None:
        pool_size = custom_pool_size
    print("Parallelize pool size:", pool_size)
    with pool_func(pool_size) as pool:
        pooling_args["pool"] = pool
        map_result = None
        map_func = get_map_func(parallel_scheme, pooling_args)               
        print("Mapping...")
        map_result = map_func.map(parallel_func, arg_list)
        print("Done with map...")
        try:
            if map_func is not None:
                print("Attempting join... 1")
                map_func.join()
                print("Join 1 Done...")
                map_func.close()    #TODO: Make sure this closes the CondorPool threadpool too
                print("Closed 1 Done.")
        except:
            print("Join 1 failed")

    
    return [x for x in map_result]

### This executes all of the chromosomes and collects the results
def exec_map(chrom_props, dataset_index, pooling_args, eval_results, parallel_scheme, parallel_job_limit=None):
    aggregate = True
    if parallel_scheme=="condor":
        # pool_func = CondorPoolDAG
        pool_func = partial(CondorPoolDAG, sub_pool_size=pooling_args["parallel_sub_dag_n"], error_path=pooling_args["error_path"])
        aggregate = False
    elif parallel_scheme=="docker":
        pool_func = DockerPool
        pooling_args["return_func"] = docker_return_func
    else:
        pool_func = ThreadPool

    if parallel_job_limit is not None:
        pooling_args = dict(pooling_args) #duplicating for safety
        if aggregate:
            pooling_args["parallel_sub_dag_n"] = parallel_job_limit
        else:
            pooling_args["parallel_dag_n"] = parallel_job_limit

    if aggregate:   #ie running chromosome by chromosome and then consolidating
        # [ [chrom1 list of arg_lists ]  ]
        arg_list = [ [case_info["args"] for case_info in chrom["dataset_info"][dataset_index]["case_infos"] ] for chrom in chrom_props if chrom["run_whole_dataset"] ]
        pool_size =  len(arg_list) if len(arg_list) < pooling_args.get("parallel_sub_dag_n", 5) else pooling_args.get("parallel_sub_dag_n", 5)
    else:           #ie running with CondorDAG
        # [ [chrom1 list of case_infos], [chrom2 list of case_infos], [chrom3 list of case_infos] ]
        arg_list = [ [chrom["dataset_info"][dataset_index] for chrom in chrom_props if chrom["run_whole_dataset"] ], ]
        pool_size =  len(arg_list[0]) if len(arg_list[0]) < pooling_args.get("parallel_dag_n", 5) else pooling_args.get("parallel_dag_n", 5)

    map_results = []
    chrom_processed = []
    for _, chr_gene_arg_list in enumerate(arg_list):
        if not chr_gene_arg_list:
            continue
        print("chr_gene_arg_list", len(chr_gene_arg_list))
        # input("parallel scheme: %s"%parallel_scheme)
        with pool_func(pool_size) as pool:
            pooling_args["pool"] = pool
            map_result = None
            map_func = get_map_func(parallel_scheme, pooling_args)
            print("exec_map dag pool size:", pool_size)
            print("exec_map sub-dag pool size:", pool_size)
            # input()
            print("Mapping...")
            map_result = map_func.map(_compute_evaluation, chr_gene_arg_list)
            print("Done with map...")
            try:
                if map_func is not None:
                    print("Attempting join... 1")
                    map_func.join()
                    print("Join 1 Done...")
                    map_func.close()    #TODO: Make sure this closes the CondorPool threadpool too
                    print("Closed 1 Done.")
            except:
                print("Join 1 failed")

        if aggregate: 
            map_results.append([x for x in map_result])
        else:
            map_results = map_result

    leftover = False
    chrom_ind = -1
    for _, m in enumerate(map_results):

        try:
        # if True:
            processed_k = []    # keep track of what's been processed for error catching
            leftover_k = []
            leftover_gene_arg_list = []
            chrom_ind+=1
            while len(chrom_props[chrom_ind]["dataset_info"][dataset_index]["keys"]) == 0:
                chrom_ind+=1
                print(".",end="")
            #TODO: This is the issue. You are trying to iterate through the range of all keys but it should be the range of keys needed to process still 
            for i,k,v in zip(range(len(chrom_props[chrom_ind]["dataset_info"][dataset_index]["keys"])), chrom_props[chrom_ind]["dataset_info"][dataset_index]["keys"], m):
                if v is not None and not isinstance(v, ErrorValue) and not isinstance(v, Exception):
                    print("=====")
                    print(i,k)
                    # print(v)
                    # print(v["nodule"][0]["a_org"])
                    # print("Success")
                    eval_results[k[0]][k[1]] = v
                    # print("...")
                    # print(v)
                    # print("...")
                    # input()
                    processed_k.append(k)
                else:
                    if v is not None:
                        print("======")
                        print("Post-Run Error:", v)
                        print(i,k)

                    leftover_k.append(k)
                    leftover_gene_arg_list.append(chrom_props[chrom_ind]["dataset_info"][dataset_index]["case_infos"][i])
                    leftover = True

        except:
        # if False:
            print("Ran into error when trying to aggregate results in second pooling attempt.")
            for i,k in zip(range(len(chrom_props[chrom_ind]["dataset_info"][dataset_index]["keys"])), chrom_props[chrom_ind]["dataset_info"][dataset_index]["keys"]):
                if k in processed_k:
                    continue
                leftover_k.append(k)
                leftover_gene_arg_list.append(chrom_props[chrom_ind]["dataset_info"][dataset_index]["case_infos"][i])
                leftover = True


        chrom_props[chrom_ind]["dataset_info"][dataset_index]["keys"] = leftover_k
        chrom_props[chrom_ind]["dataset_info"][dataset_index]["case_infos"] = leftover_gene_arg_list
    return eval_results, chrom_props, leftover

### Make sure MIU / input_param know it's using Condor or not
### by default this will toggle Condor use OFF
def toggle_condor_preprocess(chrom_props, toggle_on=False):
    for chrom_dict in chrom_props:                  # iteratest through dicts
        dataset_info = chrom_dict["dataset_info"]   # returns list
        for dataset_dict in dataset_info:           # iterates through dicts
            case_infos = dataset_dict["case_infos"] # returns list
            for case_info in case_infos:
                input_param = case_info["args"][1]
                input_param["condor"] = toggle_on   # because dictionaries are mutable then I won't make a new variable here...
    return chrom_props

def evaluate_population(population, name_input_ref_pair_lookup, outpath_func, segment_func, eval_func, compile_func, results_loader, skip=None, 
                        # map_func=None, map=map, map_join=None, map_close=None, map2=None,
                        canary_obj=None, parallel_scheme=None, pooling_args=None, error_path=None):
    """A helper function for evaluating a population. Meant to be as an argument for new_toolbox().
    :param population: The population that is to be evaluated.
    :param name_input_ref_pair_lookup: A dictionary keyed via a name string and a tuple containing information of the input and reference.
        The name will be used for creating the work directory needed to store results.
        The input and reference can be of any type as long as it is a valid input for segment_func and eval_func.
    :param result_root: A string containing the path for storing all results (individual directories will be created for each gene).
    :param segment_func: A function to perform the actual segmentation, expects the following parameters:
        gene_str: A string containing the gene.
        input: The input for the segmentation.
        segment_outpath: A string containing path where segmentation results should be stored.
    :param eval_func: A function to evaluate the segmentation results, expects the following parameters:
        segment_func_return_value: The returned value from segment_func.
        reference: The reference for comparison with the segmentation results.
        eval_outpath: Output path where the evaluation results are stored.
    :param compile_func: A function that expects a dictionary containing the evaluation results (from a single gene), a directory where results are stored and returns the fitness values (in the form of a tuple).
    :param map: "map" function for parallization process. In the case if map is related to Condor, segment_func and eval_func CANNOT be a lambda function or partial object.
    :param map_join: A "join" operation that is to be called in case of errors in "map", so executions can be safely passed to "map2"
    :param map2: A backup map function in case an error occured in the first "map" function.
    :param assess_perf: give a True if performance is satisfactory; False if unsatisfactory. Meant to operate on the canary subset.
    """
    # TODO: if bad performance, then set chrom["run_whole_dataset"] to be False
    if pooling_args is None:
        pooling_args = {}
    parallel_scheme = list(parallel_scheme)
    if parallel_scheme is None:
        parallel_scheme = ["docker_sequential", "docker_sequential"]
    parallel_limit = []
    for i, p in enumerate(parallel_scheme):
        if p=="docker_sequential":
            parallel_limit.append(1)
            parallel_scheme[i] = "docker"
        else:
            parallel_limit.append(None)
    
    eval_results = defaultdict(dict)
    fitness_lookup = {}

    canary_subset = None
    if canary_obj is not None:
        canary_subset = canary_obj.canary_data

    #Generating needed arguments for map
    [print(binlist_to_hexstr(i)) for i in population]
    print(len(population), "chromosomes.")
    # input()
    datasets = ( (canary_subset, "canary"), (name_input_ref_pair_lookup, "full") )
    chrom_props = []
    for chrom in population:
        gene_input = binlist_to_hexstr(chrom)
        print(" ",gene_input)
        gene_str = gene_input
        if gene_input is None:
            gene_str = "DEFAULT"

        dataset_info = [] 

        for dataset in datasets:
            if not dataset[0]:
                dataset_dict = dict(args=[], keys=[], case_infos=[])
                dataset_info.append(dataset_dict)
                continue    #check this
            value = list(dataset[0].values())[0]
            result_root = value[0]["results_dir"]
            compile_file = os.path.join(result_root, gene_str, "final.yml") 
            already_compiled = os.path.exists(compile_file)
            if already_compiled:
                dataset_dict = dict(already_compiled=already_compiled, compile_file=compile_file, result_root=result_root, case_infos=[], keys=[], )
                dataset_info.append(dataset_dict)
                continue    #check this
            case_infos = []
            keys = []

            ### Loading previously finished evaluations in parallel 
            parallel_args = []
            names = list(dataset[0].keys())
            for name in names:
                input_param = dataset[0][name][0]
                input_param["gene"] = gene_str
                output = outpath_func(input_param)
                case_args = [results_loader, output]
                parallel_args.append(case_args)
            print("Retrieving evaluation results in parallel:")
            print("Parallel Workers:", pooling_args["parallel_misc_n"])
            map_result = parallelize(_get_evaluation_result, parallel_args, parallel_scheme="threading", custom_pool_size=pooling_args["parallel_misc_n"]) 
            jobs_to_run = 0
            for m_index, v in enumerate(map_result):
                name = names[m_index]
                if v is not None and not isinstance(v, ErrorValue):
                    # print("Success")
                    eval_results[gene_str][name] = v  
                else:
                    # print("Error:", v)
                    jobs_to_run+=1
                    if v is not None:
                        input("Failure check.")
                    input_param = dataset[0][name][0]
                    input_param["gene"] = gene_str
                    ref = dataset[0][name][1]
                    output = outpath_func(input_param)
                    
                    args = [gene_input, input_param, ref, output, segment_func, eval_func, results_loader, skip, False]
                    hierarchy = int(input_param["hierarchy"])
                    gpus = int(input_param["gpus"])
                    gpu_memory = int(input_param["gpu_memory"])
                    case_infos.append(dict(args=args, hierarchy=hierarchy, gpus=gpus, gpu_memory=gpu_memory))
                    # sum(len(chrom_props["dataset_info"][dataset_index]["case_infos"]["args"])
                    # arg_list = sum([len(chrom["dataset_info"][dataset_index]["case_infos"]) for chrom in chrom_props if chrom["run_whole_dataset"] ])

                    keys.append((gene_str, name))
            print("::::: Jobs to run for this chromosome:", jobs_to_run)
            dataset_dict = dict(already_compiled=already_compiled, compile_file=compile_file, result_root=result_root, case_infos=case_infos, keys=keys)
            dataset_info.append(dataset_dict)
        chrom_dict = dict(run_whole_dataset=True, dataset_info=dataset_info) 
        chrom_props.append(chrom_dict)
        # skip if run_whole_dataset is False
    print("1. Chrom_props length:", len(chrom_props))

    # if parallel in ("condor_only",):
    #     parallel_schemes = ("condor", "condor")
    # elif parallel in ("condor_docker_threading",):
    #     parallel_schemes = ("condor", "docker")
    # elif parallel in ( "test", "docker_sequential", "docker_threading"):
    #     parallel_schemes = ("docker", "docker")
    # elif parallel in ("condor_threading",):
    #     parallel_schemes = ("condor", "threading")
    # else:
    #     parallel_schemes = ("threading", "default")


    # do this for both datasets
    for dataset_index, dataset in enumerate(datasets):
        if not dataset[0]:
            continue
        print("2. Number of total jobs:", sum([len(chrom["dataset_info"][dataset_index]["case_infos"]) for chrom in chrom_props if chrom["run_whole_dataset"] ]) )

        eval_results, chrom_props, leftover = exec_map(chrom_props, dataset_index, pooling_args, eval_results, parallel_scheme[0], parallel_limit[0])

        # TODO: Reactivate when debugging
        # input("Waiting...........1298")
        print("3. Number of total jobs:", sum([len(chrom["dataset_info"][dataset_index]["case_infos"]) for chrom in chrom_props if chrom["run_whole_dataset"] ]) )

        if leftover:
            print("Some cases unfinished")
            if parallel_scheme[1]=="docker_sequential":
                toggle_condor_preprocess(chrom_props)
            eval_results, chrom_props, leftover = exec_map(chrom_props, dataset_index, pooling_args, eval_results, parallel_scheme[1], parallel_limit[1])

        print("4. Number of total jobs:", sum([len(chrom["dataset_info"][dataset_index]["case_infos"]) for chrom in chrom_props if chrom["run_whole_dataset"] ]) )

        ## now check if it worked
        print("Populating results...", end='')

        if leftover:
            print("Individually processing leftover", sum([len(chrom["dataset_info"][dataset_index]["case_infos"]) for chrom in chrom_props if chrom["run_whole_dataset"] ]), "cases..")
            if parallel_scheme[0] in ("condor", "docker"):
                toggle_condor_preprocess(chrom_props)
                eval_results, chrom_props, leftover = exec_map(chrom_props, dataset_index, pooling_args, eval_results, "docker", 1)
            else:
                eval_results, chrom_props, leftover = exec_map(chrom_props, dataset_index, pooling_args, eval_results, "default", 1)
            print("Finished individual processing")
        print("5. Number of total jobs:", sum([len(chrom["dataset_info"][dataset_index]["case_infos"]) for chrom in chrom_props if chrom["run_whole_dataset"] ]) )

        rerun_compiles = []
        ### Compiling results, chromosome by chromosome
        for chrom_ind, chrom in enumerate(population):
            gene_str = binlist_to_hexstr(chrom)
            if gene_str is None: gene_str = "DEFAULT"
            rerun_compile = False   # to rerun cases that didn't run for already compiled cases
                                    # non-functional with the following lines


            already_compiled = chrom_props[chrom_ind]["dataset_info"][dataset_index]["already_compiled"]
            if not already_compiled:
                #double check to see all cases in the gene were processed  
                for name, (input_param, ref) in dataset[0].items():
                    if not eval_results.get(gene_str, {}).get(name, []):
                        input_param["gene"] = gene_str
                        input_param["condor"] = False
                        output = outpath_func(input_param)
                        print(output["eval_file"])
                        results = results_loader(output)
                        result = results.eval_out()

                        # 11182019
                        if result is None:
                            print("Rerunning", name)
                            rerun_compile = True
                            gene_arg_list = ([gene_str, input_param, ref, output, segment_func, eval_func, results_loader, skip, rerun_compile],)                         
                            toggle_condor_preprocess(chrom_props)
                            if parallel_scheme[0] in ("condor", "docker"):
                                # brute force, start a single dockerpool for each 
                                with DockerPool(1) as pool:
                                    pooling_args["pool"] = pool #update the sequential docker pool
                                    map_func = get_map_func("docker", pooling_args)
                                    result = map_func.map(_compute_evaluation, gene_arg_list)
                                    print("===result after brute force map===")
                                    print([x for x in result])
                                    map_func.close()
                                    results = results_loader(output)
                                    result = [ results.eval_out(), ]

                                    print("===result after brute force map + load evaluation===")
                                    print(result)
                                    print("================================")
                            else:
                                result = map(_compute_evaluation, gene_arg_list) #brute force
                        else:
                            result = [result,]

                        for v in result:
                            eval_results[gene_str][name] = v
                print(gene_str)
                print("Compiling results...", end='')
            rerun_compiles.append(rerun_compile)
        ######### TODO: for parallelization
        ## Prep for parallelization ##
        parallel_args = []
        print("[Before compilation]")
        for chrom_ind, chrom in enumerate(population):
            canary = True if (dataset[1]=="canary") else False
            gene_str = binlist_to_hexstr(chrom)
            if gene_str is None: gene_str = "DEFAULT"
            rerun_compile = rerun_compiles[chrom_ind]
            print("Chromosome:", gene_str, len(eval_results[gene_str]))
            parallel_args.append( [compile_func, eval_results[gene_str], os.path.join(chrom_props[chrom_ind]["dataset_info"][dataset_index]["result_root"], gene_str), rerun_compile, canary] )
        print("Compiling", len(parallel_args), "chromosomes")
        print("Parallel Workers:", pooling_args["parallel_misc_n"])
        map_result = parallelize(_compile, parallel_args, parallel_scheme="threading", custom_pool_size=pooling_args["parallel_misc_n"]) 
        for m_index, v in enumerate(map_result):
            if v is isinstance(v, ErrorValue):
                v = _compile(parallel_args[m_index])
            chrom = population[m_index]
            gene_str = binlist_to_hexstr(chrom)
            fitness_value = v[0]
            fitness_lookup[gene_str] = v
            if dataset[1]=="canary":
                chrom_props[m_index]["run_whole_dataset"] = canary_obj.assess_perf(fitness_value)
                if not chrom_props[m_index]["run_whole_dataset"]:
                    print("=============")
                    print("CANARY SUBSET DID NOT PASS!", gene_str)
                    print("=============")
                    fitness_lookup[gene_str] = 0
                else:
                    print("Passed Canary:", gene_str)

    print("Done!")
    # exit()
    for i in population:
        if i is None:
            print("No-gene evaluation finished.")
            continue
        i.fitness.values = fitness_lookup[binlist_to_hexstr(i)]

        