import csv, os, yaml, glob 
import numpy as np
import importlib

## basic reader for reading case list from case .csv file
def load_input_ref(input_file, input_func, ref_func):
    ret = {}    
    with open(input_file, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            ret[row["id"]] = (input_func(row), ref_func(row))
    return ret
    
## Defines input parameters from case .csv file
def _gen_input(case_dict, model_file=None, senn=False, os_env="linux", skip=None, skip_images=False, cpu_preprocessing_only=False, parallel=None):
    if parallel is None:
        parallel = dict()
    return {
        "id": case_dict["id"],                          #what is this for LCS -- probably the recon_id + the reconstruction params
        # "edm": None if edm_path is None else os.path.join(edm_path, case_dict["id"]),
        "image_file": case_dict["image_file"],
        "model": model_file,
        "reference": case_dict["reference"],
        "dataset": case_dict["dataset"],
        "results_dir": case_dict["results_dir"],              #case directory for results
        "roi_dir": case_dict.get("roi_dir"),                  #case directory for premade input rois  # MWW 09162020
        "working_dir": case_dict.get("working_dir"),          #working directory for preprocessing  # MWW 01302021
        "resource_dir": case_dict.get("resource_dir", ""),    #working directory for preprocessing  # MWW 01302021
        "hierarchy": case_dict.get("hierarchy", 0),           #hierarchy for processing  # MWW 02242021
        "gpus": case_dict.get("gpus", 0),                     #how many gpus to reserve on Condor  # MWW 02242021
        "gpu_memory": case_dict.get("gpu_memory", 0),         #minimum gpu memory needed (GB)  # MWW 05012021
        "senn": senn,                                         #semantic enhanced
        "os_env": os_env,                                     #environment: "linux", "ad", "ldap"
        "condor": parallel.get("condor_only", False),
        "skip": skip,                                         #whether we're skipping processing of anything
        "cpu_preprocessing_only": cpu_preprocessing_only,           #whether we're skipping processing of anything
        "skip_images": skip_images,                           #### LEGACY #### whether we're skipping processing of images (legacy)
    }

## Defines output parameters
# General version, that can be overriden 
# TODO: Questions
# - is seg_img necessary for GA -- only used in _compute_evaluation in ga_2_0.py
def _gen_output(case_dict):
    chrom_str = "DEFAULT"
    if case_dict.get("gene", ""):
        chrom_str = case_dict.get("gene", "")
    gene_results_dir = os.path.join(case_dict["results_dir"], chrom_str)
    seg_file = os.path.join(gene_results_dir, case_dict["id"], "seg_res.yml")
    log_file = os.path.join(gene_results_dir, case_dict["id"], "log", "seg_log.txt")
    seg_dir = os.path.join(gene_results_dir, case_dict["id"], "seg")
    seg_img = os.path.join(seg_dir, "dicom.seri")
    seg_done_file = os.path.join(seg_dir, "file_list.txt")
    seg_error_logs = [os.path.join(seg_dir, "error_log_*.log"), ]     # MWW 09202021
    eval_dir = os.path.join(gene_results_dir, case_dict["id"], "eval")
    eval_file = os.path.join(gene_results_dir, case_dict["id"], "eval_res.yml")
    return {    "id": case_dict["id"],
                "log_file": log_file,
                "seg": seg_dir,
                "seg_done": seg_done_file,
                "seg_error_logs": seg_error_logs,
                "seg_img": seg_img,
                "seg_res": seg_file,
                "eval": eval_dir,
                "eval_file": eval_file,
                "gene_results_dir": gene_results_dir,
            }

## TODO: Make this general
def _gen_ref(case):
    ref = None
    if case["dataset"]=="lidc":
        ref = os.path.join(case["reference"], case["id"])
        if not os.path.exists(ref):
            raise IOError("Cannot find %s" % ref)
    elif case["dataset"]=="cxr":
        ref = os.path.join(case["reference"], case["id"])
        if not os.path.exists(ref):
            raise IOError("Cannot find %s" % ref)
    else:
        if os.path.exists(case["reference"]):
            ref = case["reference"]
    return ref

def get_ga_param(conf_ga=None, vec_size=None, pop_size=None, gene=None, no_gene=False):
    # :param ga_param: A dictionary with the following keys:
        # vec_size: number of bits per gene
        # pop_size: numper of individuals per population
        # t_size: tournament size
        # p_mut_per_bit: probably of mutation per bit
        # cxpb: crossover probability
        # mutpb: mutation probability
        # max_gen: maximum evolution generation
        # creator_individual: individual from deap.creator
        # seed: [OPTIONAL] random seed
        # stats: [OPTIONAL] value of type deap.tools.Statistics
        # halloffame: [OPTIONAL] value of type deap.tools.HallOfFame
    
    # 
    from deap import creator
    from deap import base
    from deap import tools
    
    
    if conf_ga is not None:
        with open(conf_ga, 'r') as f:
            ga_param = yaml.load(f, Loader=yaml.FullLoader)
        ga_param["evolve_genes"] = ga_param.get("evolve_genes", True)
        ga_param["init_genes"] = ga_param.get("init_genes", ())
        if ga_param["init_genes"] is None: ga_param["init_genes"] = ()
        ga_param["init_process_n"] = ga_param.get("init_process_n", 0)
        ga_param["vec_size"] = ga_param.get("chrom_length", ga_param.get("vec_size"))   # looks for "chrom_length" and if not then just use what's in "vec_size"
        if ga_param.get("skip") is not None:
            skip = dict()
            for k in ga_param["skip"]:
                skip[k] = True
            ga_param["skip"] = skip 
        ga_param["evolve_genes"] = ga_param.get("evolve_genes", True)
    else:
        ga_param = {}
        ga_param["seed"] = 42
        # ga_param["vec_size"] = 39
        ga_param["pop_size"] = 10
        ga_param["t_size"] = 3
        ga_param["p_mut_per_bit"] = .01
        ga_param["cxpb"] = .3
        ga_param["mutpb"] = 1.0
        ga_param["max_gen"] = 10
        ga_param["init_genes"] = []
        ga_param["gene_hex"] = False
        ga_param["evolve_genes"] = True
    if gene is not None:
        ga_param["init_genes"] = [gene,]
        ga_param["evolve_genes"] = False
    if no_gene:
        ga_param["init_genes"] = [None,]
        ga_param["evolve_genes"] = False

    creator.create("FitnessMax", base.Fitness, weights=(1.0, 1.0))
    creator.create("Individual", list, fitness=creator.FitnessMax)
    ga_param["creator_individual"] = creator.Individual

    if ga_param.get("include_stats", True):
        stats = tools.Statistics(key=lambda ind: ind.fitness.values)
        stats.register("avg", np.mean)
        stats.register("std", np.std)
        stats.register("min", np.min)
        stats.register("max", np.max)
        ga_param["stats"] = stats
    if vec_size is not None:
        ga_param["vec_size"] = vec_size
    if pop_size is not None:
        ga_param["pop_size"] = pop_size
        
    ga_param.update(load_task_specific_functions(ga_param))
    ga_param.update(function_setup(ga_param))
    
    return ga_param

def load_task_specific_functions(ga_param):
    loaded_funcs = dict()
    if ga_param.get("import") is not None:
        for filepath, funcs in ga_param["import"].items():
            print(filepath)
            module = importlib.import_module(filepath)
            for tar_func, orig_func in funcs.items():
                loaded_funcs[tar_func] = getattr(module,  orig_func)
    return loaded_funcs

def function_setup(ga_param):
    # if ga_param.get("Evaluator") is not None:
    #     evaluator = ga_param["Evaluator"]()
    #     ga_param["evaluate_task"] = evaluator.evaluate_task
    if ga_param.get("Compiler") is not None:
        compiler = ga_param["Compiler"]()
        ga_param["compile_function"] = compiler.compile_function
    
    ### potentially something here about Canary dataset
     
    return ga_param

class BaseResultsLoader():
    def __init__(self, output):
        self.output=output
    def seg_finished(self):
        
        if not os.path.exists(self.output['seg_img']):
            img_path = os.path.join(os.path.split(self.output['seg_img'])[0], "source_image.txt")
            with open(img_path, 'r') as f:
                image_path = f.read()
            self.output['seg_img'] = image_path

        print("seg_img:", self.output.get("seg_img", ""))
        print("seg_done:", self.output.get("seg_done", ""))
        no_error = True 
        error_logs = self.output.get("seg_error_logs")
        if error_logs is not None:
            for error_log in error_logs:
                print("Looking for...", error_log)
                if "*" in error_log:
                    detected_errors = glob.glob(error_log)
                    if detected_errors: 
                        no_error = False
                else:
                    if os.path.exists(error_log):
                        no_error = False 
                print("No errors? ", no_error)
        return os.path.exists(self.output.get("seg_img", "")) and os.path.exists(self.output.get("seg_done", "")) and no_error
    def seg_out(self, load_results=True):
        seg_out = None
        if not os.path.exists(self.output.get("seg_res", "")):
            return seg_out
        else:
            if load_results:
                print("Seg already finished...loading output")
                with open(self.output["seg_res"]) as f:
                    ret = yaml.load(f)
                if ret:
                    seg_out = ret
            else:
                seg_out = True
        return seg_out
    def eval_out(self, load_results=True):
        eval_out = None
        if os.path.exists(self.output.get("eval_file", "")):
            if load_results:
                print("Loading eval...", self.output["eval_file"])
                with open(self.output["eval_file"]) as f:
                    ret = yaml.load(f)
                if ret: # only checks that ret is existent... nothing about the contents
                    eval_out = ret
            else:
                eval_out = True
        return eval_out


        


    