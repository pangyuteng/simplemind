import os
from functools import partial
import time
from simplemind.apt_agents.optimizer.ga.src.core.ga import new_toolbox, evolve, evaluate_population, clean 
from simplemind.apt_agents.distributor.condor.src.qia.scriptmaker import get_import_statement
from simplemind.apt_agents.distributor.condor.src.pool import argshash_workpath_generator, default_creator
    

### default hashfunc for reference  ###
# def hashfunc(x):
#     t = hashlib.new('ripemd160')
#     t.update("|".join([repr(i) if not isfunction(i) else i.__name__ for i in x]).encode('utf-8'))
#     return t.hexdigest()

def ga_hashfunc(x):
    gene_str = x[0]
    input_info = x[1]
    epoch_time = str(int(time.time()))
    return "%s_%s_%s"%(gene_str, input_info.get("id", "NA"), epoch_time)

    
def condor_creator(condor_script, exec_script, condor_venv="", os_env="ldap"):
    if os_env=="linux":
        job = default_creator(condor_script, exec_script, condor_venv=condor_venv, linux=True)
    else:
        job = default_creator(condor_script, exec_script, condor_venv=condor_venv)
        job["requirements"].append("CVIB==TRUE")
    return job

def filler_func(*args, **kargs):
    return 


        

def main(name_input_ref_pair_lookup, checkpoint, ga_param, outpath_func, segment_func, eval_func, compile_func, results_loader,
            computing_management=None, canary_obj=None):

    """A helper main function for the performing the GA evolution.

    :param workpath: A folder where all results are stored.
    :param name_input_ref_pair_lookup: A dictionary keyed via a name string and a tuple containing information of the input and reference.
        The name will be used for creating the work directory needed to store results.
        The input and reference can be of any type as long as it is a valid input for segment_func and eval_func.
    :param ga_param: A dictionary with the following keys:
       vec_size: number of bits per gene
       pop_size: numper of individuals per population
       t_size: tournament size
       p_mut_per_bit: probably of mutation per bit
       cxpb: crossover probability
       mutpb: mutation probability
       max_gen: maximum evolution generation
       creator_individual: individual from deap.creator
       seed: [OPTIONAL] random seed
       stats: [OPTIONAL] value of type deap.tools.Statistics
       halloffame: [OPTIONAL] value of type deap.tools.HallOfFame
    :param segment_func: A function to perform the actual segmentation, expects the following parameters:
        gene_str: A string containing the gene.
        input: The input for the segmentation.
        segment_outpath: A string containing path where segmentation results should be stored.
    :param eval_func: A function to evaluate the segmentation results, expects the following parameters:
        segment_func_return_value: The returned value from segment_func.
        reference: The reference for comparison with the segmentation results.
        eval_outpath: Output path where the evaluation results are stored.
    :param compile_func: A function that expects a dictionary containing the evaluation results (from a single gene), a directory where results are stored and returns the fitness values (in the form of a tuple).

    Returns a tuple containing the population from the last generation and a deap.tools.Logbook object.
    """
    if computing_management is None:
        computing_management = dict(os_env="linux", condor_venv=None, condor_workpath="/scratch2/personal/wasil/trash/condor_run/temp", error_path=None, parallel=None, parallel_dag_n=1, parallel_sub_dag_n=1, timeout=None)
    os.makedirs(computing_management["condor_workpath"], exist_ok=True)
    workpath_generator = partial(argshash_workpath_generator, hashfunc=ga_hashfunc, rootpath=computing_management["condor_workpath"])
    prepcode = [
        get_import_statement(segment_func),
        get_import_statement(eval_func),
    ]
    pooling_args = dict(workpath_generator=workpath_generator, creator=condor_creator, prepcode=prepcode, 
                        parallel_dag_n=computing_management["parallel"]["parallel_dag_n"], parallel_sub_dag_n=computing_management["parallel"]["parallel_sub_dag_n"], parallel_misc_n=computing_management["parallel"]["parallel_misc_n"], 
                        condor_venv=computing_management["condor_venv"], os_env=computing_management["os_env"], timeout=computing_management["timeout"], error_path=computing_management["error_path"])
    eval_pop = partial(
        evaluate_population, 
        name_input_ref_pair_lookup=name_input_ref_pair_lookup, outpath_func=outpath_func,
        segment_func=segment_func, eval_func=eval_func, compile_func=compile_func, results_loader=results_loader, 
        skip=ga_param.get("skip"),
        parallel_scheme=computing_management["parallel"]["parallel_scheme"], pooling_args=pooling_args, error_path=computing_management["error_path"])

    if ga_param.get("compact", False):
        print("Compact mode ON.")
        cleaner = partial(clean, name_input_ref_pair_lookup=name_input_ref_pair_lookup, local_workers=computing_management["parallel"]["parallel_misc_n"])
    else:
        cleaner = partial(filler_func)

    tb = new_toolbox(ga_param["creator_individual"], eval_pop, ga_param, cleaner)
    tb.canary = canary_obj
    return evolve(ga_param, ga_param["max_gen"], tb, stats=ga_param.get("stats"), checkpoint=checkpoint, seed=ga_param.get("seed"), halloffame=ga_param.get("halloffame"), genes=ga_param.get("init_genes", ()),evolve_genes=ga_param["evolve_genes"], gene_hex=ga_param["gene_hex"], )




