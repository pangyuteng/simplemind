import os
import pickle
import yaml
import subprocess
import threading
import sys
from simplemind.apt_agents.distributor.condor.src.creator import new_bat, new
from simplemind.apt_agents.distributor.condor.src.utils import Cluster, iter_log, set_dag_job_hierarchy
import time

import simplemind.apt_agents.distributor.condor.src.qia.scriptmaker as sm
from simplemind.apt_agents.distributor.condor.src.qia.temp import get_temp_dir
from simplemind.apt_agents.distributor.condor.src.qia.threadpool import ThreadPool, _AsynResultContainer
from simplemind.apt_agents.distributor.condor.src.qia.exceptions import NonZeroExitCode, TimedOut, ErrorValue, NotReady


def argshash_workpath_generator(func, args, rootpath, hashfunc, dag=False):
    if not dag:
        arghash = hashfunc(args)
        return os.path.join(rootpath, "%s-%s" % (func.__name__, arghash))
    else:
        arghash = hashfunc(args)
        return os.path.join(rootpath, "dag", "%s-%s.condor" % (func.__name__, arghash))

# TODO: to handle DAG -- mainly for get ??? not sure if it's necessary
class _AsynResult:
    def __init__(self, workpath):
        self.workpath = workpath
        self._value = None
        self._event = threading.Event()
        self._event.clear()
        
    def set(self, val):
        # print(val)
        # print(self._event)
        self._value = val
        self._event.set()
        
    def get(self, timeout=None):
        if self._event.wait(timeout):
            return self._value
        else:
            raise TimedOut

def default_creator(condor_script, exec_script, condor_venv="", os_env="ldap"):
    job = new_bat(condor_script, venv_bat=condor_venv, os_env=os_env)
    return job
    
def _delete_file(file):
    while os.path.exists(file):
        try:
            os.remove(file)
        except:
            time.sleep(0.1)

## Simple object to make result consistent with map() operation ##
class DAGSubResult:
    def __init__(self, workpath):
        self.obj = sm.ScriptObject(workpath)
    def get(self):
        return self.obj.value
        
# each job is encapsulated in this object
class _ScriptJob:
    def __init__(self, workpath, prepcode, func, args, kwargs, creator, condor_venv="", os_env="linux", watcher_dir=None, error_path=None):
        self.workpath = workpath
        self.creator = creator
        self.process = None
        self.condor_venv = condor_venv    #MWW 07272020
        self.os_env = os_env    #MWW 07272020
        self._submitted = False
        if self._exists(func, args, kwargs):
            self.obj = sm.ScriptObject(self.workpath)

            # what is this doing
            # probably recording which cluster it's in, so we can keep track of the job 
            if os.path.exists(os.path.join(self.workpath, "cluster")):
                with open(os.path.join(self.workpath, "cluster"), "r") as f:
                    cluster = Cluster(int(f.read()), watcher_dir=watcher_dir)
                for p in cluster.get_processes():
                    self.process = p
                    break
        else:
            _delete_file(os.path.join(self.workpath, sm.OUTPUT_FILENAME))
            _delete_file(os.path.join(self.workpath, sm.ERROR_FILENAME))
            _delete_file(os.path.join(self.workpath, "cluster"))
            self.obj = sm.make(self.workpath, prepcode, func, *args, **kwargs)
        
    def _exists(self, func, args, kwargs):
        if not (os.path.exists(os.path.join(self.workpath, sm.SCRIPT_FILENAME) or not os.path.exists(os.path.join(self.workpath, sm.INPUT_FILENAME)))):
            return False
        if os.path.exists(os.path.join(self.workpath, sm.SCRIPT_FILENAME)):
            try:
                if isinstance(sm.retrieve(self.workpath), ErrorValue):
                    return False
            except EOFError:
                return False
        return sm.is_same(self.workpath, func, *args, **kwargs)
        
    def done(self):
        outfile = os.path.join(self.workpath, sm.OUTPUT_FILENAME)
        if os.path.exists(outfile) and os.stat(outfile).st_size>0:
            return True
        return False
        
    def submit(self):   #TODO: implement GPU options here (how many GPUs, minimum GPU memory) for normal condor usage too 
        if self._submitted:
            return
        self._submitted = True
        if self.done():
            return
        if self.process is not None:
            if self.process.is_active():
                return
            self.remove()
        condor_script = os.path.join(self.workpath, "script.condor")
        job = self.creator(condor_script, self.obj.script , condor_venv=self.condor_venv, os_env=self.os_env) #MWW 07272020
        job["transfer_input_files"].append(os.path.join(os.path.dirname(self.obj.script), sm.INPUT_FILENAME))
        job["transfer_input_files"].append(os.path.join(os.path.dirname(self.obj.script), sm.SCRIPT_FILENAME))
        job["transfer_output_files"].append(sm.OUTPUT_FILENAME)
        cluster = job.submit()
        # print(cluster)
        self.process = cluster.get_processes()[0]
        # print(os.path.join(self.workpath, "cluster"))

        with open(os.path.join(self.workpath, "cluster"), "w") as f:
            f.write(str(cluster.id))
        
    def wait(self, timeout=None, retrynum=None):   #MWW 07012020 change timeout to None
        if self.done():
            print("Output is finished preparing. Done waiting.")
            return
        if self.process is None:
            raise ValueError("No process to wait on!")
        retry_count = 0
        while True:
            try:
                print("WAITING ATTEMPT >>> timeout is", timeout)
                self.process.wait(timeout)
                break
            except TimedOut:
                ## old as of 07012020
                # if retrynum is not None and retry_count>=retrynum:
                #     raise
                # retry_count += 1

                ## new 07012020
                print("Timed out in _ScriptJob")
                # input()
                # if retrynum is not None and retry_count>=retrynum:
                # raise
                # retry_count += 1
                break
                # pass
            except NonZeroExitCode:
                ### This should happen if there is a problem with the GA Watcher sub-job.
                print("Fatal error from job or sub-job.")
                raise
            except:
                print("Unexpected error:", sys.exc_info()[0])
                raise

        # log_file = self.process.log
        # if log_file is None:
        #     log_file = os.path.join(self.workpath, "log", "%s.%s.log" % (self.process.id, self.process.pid))
        #     if not os.path.exists(log_file):
        #         return
        # info = None
        # for i in iter_log(log_file, id=self.process.id, pid=self.process.pid):
        #     info = i
        # if info["title"]=="Job terminated.":
        #     if info["content"][0].find("return value 0")>=0:
        #        pass
        #     else:
        #         raise NonZeroExitCode

        log_file = self.process.log
        if log_file is None:
            log_file = os.path.join(self.workpath, "log", "%s.%s.log" % (self.process.id, self.process.pid))
            if not os.path.exists(log_file):
                print("Log file does not exist!")
                return
        info = None
        for i in iter_log(log_file, id=self.process.id, pid=self.process.pid):
            info = i
        if info["title"]=="Job terminated.":
            if info["content"][0].find("return value 0")>=0:
               pass
            else:
                print("WAITING JOB TERMINATED, NonZeroExitCode raised.")
                raise NonZeroExitCode #should this be raised? if so, it should be caught somewhere
        print("WAITING FINISHED")


    def remove(self):
        if self.process is not None:
            self.process.remove()
            self.process = None
        
    def retrieve(self):
        return self.obj.value
        
    def create(self, gpus=0, gpu_memory=0):
        if self._submitted:
            return
        # self._submitted = True
        if self.done():
            return
        if self.process is not None:
            if self.process.is_active():
                return
            print("Removing due to inactivity, in 'create' method.")
            self.remove()
        condor_script = os.path.join(self.workpath, "script.condor")
        job = self.creator(condor_script, self.obj.script , condor_venv=self.condor_venv, os_env=self.os_env) #MWW 07272020
        job["transfer_input_files"].append(os.path.join(os.path.dirname(self.obj.script), sm.INPUT_FILENAME))
        job["transfer_input_files"].append(os.path.join(os.path.dirname(self.obj.script), sm.SCRIPT_FILENAME))
        job["transfer_output_files"].append(sm.OUTPUT_FILENAME)

        if gpus>0:
            job["request_gpus"] = gpus
        else:
            job["request_gpus"] = 0
        if gpu_memory>0:    #assume GPU memory is given in GB, so convert it to MB
            gpu_memory*= 1000
            ### requirements will now be in format of:
            # Requirements = ((Machine == "REDLRADADM14958.ad.medctr.ucla.edu") || (Machine  == "REDLRADADM23710.ad.medctr.ucla.edu") ) && (GPUMEM >= 46000)
            job["requirements"] = "(%s) && (%s)"%(job["requirements"], "GPUMEM >= %s"%(str(gpu_memory)) )
        else:
            if gpus> 0:
                # input("NO MEMORY")
                print("No GPU memory requirements")
        return job.create()


    ### Not ready to use ###
    def find_cluster(self):
        ## what if i don't have the cluster file
        if os.path.exists(os.path.join(self.workpath, "cluster")):
            with open(os.path.join(self.workpath, "cluster"), "r") as f:
                cluster = Cluster(int(f.read()))
            for p in cluster.get_processes():
                self.process = p
                break


class _ScriptCmdlineJob:
    def __init__(self, workpath, command, creator, template=True, auto_mount_exec=None, auto_mount_conf=None, copy_automount=False):
        self.workpath = workpath
        self.command = command
        self.creator = creator
        self.template = template
        self.auto_mount_exec = auto_mount_exec
        self.auto_mount_conf = auto_mount_conf
        self.copy_automount = copy_automount
        self.exitcode = None
        self.process = None
        self._submitted = False
        if self._exists():
            if os.path.exists(os.path.join(self.workpath, "cluster")):
                with open(os.path.join(self.workpath, "cluster"), "r") as f:
                    cluster = Cluster(int(f.read()))
                for p in cluster.get_processes():
                    self.process = p
                    break;
            self._execution_success()
        else:
            if not os.path.exists(self.workpath):
                os.makedirs(self.workpath)
            with open(os.path.join(self.workpath, "command"), "w") as f:
                f.write(command)
            _delete_file(os.path.join(self.workpath, "cluster"))
            _delete_file(os.path.join(self.workpath, "done"))
    
    def _exists(self):
        if os.path.exists(os.path.join(self.workpath, "command")):
            with open(os.path.join(self.workpath, "command"), "r") as f:
                if f.read()==self.command:
                    return True
        return False
        
    def done(self):
        if os.path.exists(os.path.join(self.workpath, "done")):
            return True
        return False
    
    def submit(self):
        if self._submitted:
            return
        self._submitted = True
        if self.done():
            return
        if self.process is not None:
            if self.process.is_active():
                return
            self.process.remove()
            self.process = None
        
        condor_script = os.path.join(self.workpath, "script.condor") 
        job = self.creator(
            condor_script,
            self.command,
            template=self.template,
            auto_mount_exec=self.auto_mount_exec,
            auto_mount_conf=self.auto_mount_conf,
            copy_automount=self.copy_automount,
        )        
        cluster = job.submit()
        self.process = cluster.get_processes()[0]
        with open(os.path.join(self.workpath, "cluster"), "w") as f:
            f.write(str(cluster.id))
            
    def _execution_success(self):
        if self.done():
            return
        if self.process is None:
            return
        if self.process.is_active():
            return
        log_file = self.process.log
        if log_file is None:
            log_file = os.path.join(self.workpath, "log", "%s.%s.log" % (self.process.id, self.process.pid))
            if not os.path.exists(log_file):
                return
        info = None
        for i in iter_log(log_file, id=self.process.id, pid=self.process.pid):
            info = i
        if info["title"]=="Job terminated.":
            pos = info["content"][0].find("return value")
            if pos>=0:
                self.exitcode = int(info["content"][0][pos:-1].rsplit(" ", 1)[1])
            else:
                self.exitcode = Error
            if self.exitcode==0:
               with open(os.path.join(self.workpath, "done"), "w"):
                    pass
        
    def wait(self, timeout=3, retrynum=None):
        if self.done():
            return
        if self.process is None:
            raise ValueError("No process to wait on!")
        retry_count = 0
        while True:
            try:
                self.process.wait(timeout)
                break
            except TimedOut:
                if retrynum is not None and retry_count>=retrynum:
                    raise
                retry_count += 1
                pass
        self._execution_success()
        
    def remove(self):
        if self.process is not None:
            self.process.remove()
        
#submits the actual job
# need to have this submitted more often than just 3 times
def _async_script_job_wrapper(asyn_result, job, timeout, retrynum=5, dag=False, sub_pool_size=15, watcher_dir=None, error_path=None):
    try:    #MWW 07282020
        if dag:
            job = job.submit_dag(max_jobs=sub_pool_size, watcher_dir=watcher_dir, error_path=error_path)
        else:
            job.submit()    # (1) Submit job
        print("Job Submitted:", job)
        print("Waiting 15s before checking..")
        time.sleep(15)      # MWW 11042021, to allow time for Condor dag to be submitted and ready before being checked
        print("Done waiting..")
    except NonZeroExitCode:
        print("NonZeroExitCode from job.submit() in pool.py ... Retrying ...")
        i = 0
        done = False
        while i < 5:
            try:
                print("Retrying condor job submission... Try ", i)
                # wait for 5 seconds
                time.sleep(5)
                job.submit()
                done = True
                break
            except:
                pass
            i+=1
        if not done:
            job.remove()
            asyn_result.set(ErrorValue(sys.exc_info())) #asyn result is carried on
            print("Removed after job.submit()")
            print(ErrorValue(sys.exc_info()))
            return True #unsure if this should return True or False

    try:
        interval_seconds = 15
        print("Timeout:", timeout, "Retrynum", retrynum, "interval seconds", interval_seconds)
        job.wait(timeout, interval_seconds=interval_seconds) # (2) Check status (and wait if not finished)

        # try:
            # print("Waiting.. Ln 293 pool.py")
            # job.wait(timeout, retrynum)
        # except NonZeroExitCode:
        #     print("NonZeroExitCode in Ln 293 pool.py (from job.wait) ")
        # except:
        #     print("Other error in Ln 293 pool.py (from job.wait) ")
        #     print(ErrorValue(sys.exc_info()))

        # retrieve just tries to find the open file

        if not dag:
            print("Retrieving in Ln 293 pool.py")
            print(asyn_result)
            asyn_result.set(job.retrieve()) # (3) Retrieves results from output file
            print("Done retrieving in Ln 293 pool.py")
        else:
            # retrieve the job somehow
            # potentially making a wrapper object for Process
            # but also don't really need this
            pass
            # asyn_result.set(job.retrieve())
    except TimedOut:
        print("Timed out in async_job_wrapper in pool.py, after either job.wait or job.retrieve() ")
        # input("yo")
        if not dag:
            asyn_result.set(TimedOut)
        else:
            raise(TimedOut)
    except NotReady:
        i = 0
        done = False
        while i < 5:
            try:
                print("Retrying retrieval... Try ", i)
                # wait for 5 seconds
                time.sleep(5)
                if not dag:
                    asyn_result.set(job.retrieve())
                done = True
                break
            except:
                pass
            i+=1
        if not done:
            job.remove()
            asyn_result.set(ErrorValue(sys.exc_info())) #asyn result is carried on
            print(ErrorValue(sys.exc_info()))
            #should this return False? or does it even mattter?
    ### TODO: REACTIVATE
    # except:
    #     print("Removing job. After waiting/retrieval.")
    #     print("Error", ErrorValue(sys.exc_info()))
    #     job.remove()
    #     if not dag:
    #         asyn_result.set(ErrorValue(sys.exc_info()))
            # should this return False?
    return True

r""" MWW 07012020
    try:
        job.submit()
        try:
            job.wait(timeout, retrynum)
        except NonZeroExitCode:
            pass
        asyn_result.set(job.retrieve())
    except TimedOut:
        return False
    except:
        job.remove()
        asyn_result.set(ErrorValue(sys.exc_info()))
    return True
"""

def _async_cmdline_wrapper(asyn_result, job, timeout, retrynum):
    try:
        job.submit()
        job.wait(timeout, retrynum)
        asyn_result.set(job.exitcode)
    except TimedOut:
        return False
    except:
        job.remove()
        asyn_result.set(ErrorValue(sys.exc_info()))
    return True
        
class CondorPool:
    def __init__(self, poolsize=None, workernum=None, error_callback=None, timeout=None, retrynum=1, error_path=None):
        workernum = poolsize
        print("CONDORPOOL: poolsize:", poolsize, "workernum", workernum)
        self._pool = ThreadPool(parallelnum=workernum, poolsize=poolsize, error_callback=error_callback)
        self._timeout = timeout
        self._retrynum = retrynum
        
    def __del__(self):
        self.close()

    def set_timeout(self, timeout):
        self._timeout = timeout

    # done individually for each job, within asyn result container
    # initiated via pool put (_pool is a threadpool)
    # _async_script_job_wrapper function is in charge of the actual submit + watching the job + removing when finished
    def apply(self, func, args=None, kwargs=None, timeout=None, prepcode=None, workpath=None, creator=None, condor_venv="", os_env="linux", ):
        if args is None:
            args = []
        if kwargs is None:
            kwargs = {}
        if workpath is None:
            workpath = get_temp_dir()
        if creator is None:
            creator = default_creator
        job = _ScriptJob(workpath, prepcode, func, args, kwargs, creator, condor_venv=condor_venv, os_env=os_env) #MWW 07272020
        ret = _AsynResult(workpath)
        print("Applying in condorpool async_script_job_wrapper")
        # this happens for all jobs in the beginning
        #
        self._pool.put(_async_script_job_wrapper, args=(ret, job, self._timeout, self._retrynum), timeout=timeout)
        return ret

    # not validated to work with updates
    def apply_cmdline(self, cmd, timeout=None, workpath=None, creator=None, template=True,auto_mount_exec=None,auto_mount_conf=None,copy_automount=False):
        if workpath is None:
            workpath = get_temp_dir()
        if creator is None:
            creator = default_creator
        if not isinstance(cmd, str):
            cmd = subprocess.list2cmdline(cmd)
        job = _ScriptCmdlineJob(workpath, cmd, creator,template,auto_mount_exec=auto_mount_exec,auto_mount_conf=auto_mount_conf,copy_automount=copy_automount)
        ret = _AsynResult(workpath)
        self._pool.put(_async_cmdline_wrapper, args=(ret, job, self._timeout, self._retrynum), timeout=timeout)
        return ret

    # map will get an async_map output (just a list of the functions + inputs + depenednecies)
    #   which will be iterated through in the asyn result container
    #       --> async result container will call "apply"
    def map(self, func, seq, prepcode=None, workpath=None, creator=None, workpath_generator=None, condor_venv="", os_env="ldap"):
        async_res = self.async_map(func, seq, prepcode, workpath, creator, workpath_generator, condor_venv=condor_venv, os_env=os_env)
        return _AsynResultContainer(async_res)
        
    def map_cmdline(self, seq, workpath=None, creator=None, workpath_generator=None):
        async_res = self.async_map_cmdline(seq, workpath, creator, workpath_generator)
        return _AsynResultContainer(async_res)

    #preps functions into list to be called sequentially
    # will do for all jobs in the input sent to map (in seq)
    def async_map(self, func, seq, prepcode=None, workpath=None, creator=None, workpath_generator=None, condor_venv="", os_env="ldap" ):
        if creator is None:
            creator = default_creator
        is_temp = False
        if workpath is None and workpath_generator is None:
            is_temp = True
            workpath = get_temp_dir()
        count = 0
        ret = []

        for s in seq:
            if workpath_generator is None:
                cur_wp = os.path.join(workpath, "job"+str(count))
                if is_temp:
                    cur_wp = workpath.alias(cur_wp)
            else:
                cur_wp = workpath_generator(func, s)
            ret.append(self.apply(func, args=(s,), prepcode=prepcode, workpath=cur_wp, creator=creator, condor_venv=condor_venv, os_env=os_env))
            #this is being limited by the thread count
            count += 1
            print("async_map job #", count)
        return ret
        
    def async_map_cmdline(self, seq, workpath=None, creator=None, workpath_generator=None):
        if creator is None:
            creator = default_creator
        is_temp = False
        if workpath is None and workpath_generator is None:
            is_temp = True
            workpath = get_temp_dir()
        count = 0
        ret = []
        for s in seq:
            if workpath_generator is None:
                cur_wp = os.path.join(workpath, "job"+str(count))
                if is_temp:
                    cur_wp = workpath.alias(cur_wp)
            else:
                cur_wp = workpath_generator(func, s)
            ret.append(self.apply_cmdline(s, workpath=cur_wp, creator=creator))
            count += 1
        return ret
        
    def join(self, timeout=None):
        return self._pool.join(timeout)
        
    def close(self):
        self._pool.close()
        
    def __enter__(self):
        return self
        
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
        return False



"""
Wasil removed the cmd line alternatives for cleanliness.
"""
class CondorPoolDAG:
    # Pool size will be the number of parallel chromosomes
    def __init__(self, poolsize=None, sub_pool_size=15, workernum=None, error_callback=None, timeout=None, retrynum=1, error_path=None):
        workernum = poolsize
        print("CONDORPOOLDAG: poolsize:", poolsize, "workernum", workernum, "sub_pool_size", sub_pool_size)
        self._pool = ThreadPool(parallelnum=workernum, poolsize=poolsize, error_callback=error_callback)
        self._timeout = timeout
        self._retrynum = retrynum
        self.sub_pool_size = sub_pool_size
        self.error_path = error_path
    def __del__(self):
        self.close()

    def set_timeout(self, timeout):
        self._timeout = timeout

    # done individually for each job, within asyn result container
    # initiated via pool put (_pool is a threadpool)
    # _async_script_job_wrapper function is in charge of the actual submit + watching the job + removing when finished
    # TODO: either implement timeout feature, or take it out
    def apply(self, func, args=None, kwargs=None, timeout=None, prepcode=None, workpath=None, creator=None, condor_venv="", os_env="linux", gpus=0, gpu_memory=0, watcher_dir=None):
        if args is None:
            args = []
        if kwargs is None:
            kwargs = {}
        if workpath is None:
            workpath = get_temp_dir()
        if creator is None:
            creator = default_creator

        ### Add the option to include GPU requirement in here
        job = _ScriptJob(workpath, prepcode, func, args, kwargs, creator, condor_venv=condor_venv, os_env=os_env, watcher_dir=watcher_dir, error_path=self.error_path) #MWW 07272020
        print("Creating job but not running:")
        return job.create(gpus=gpus, gpu_memory=gpu_memory)

    # map will get an async_map output (just a list of the functions + inputs + depenednecies)
    #   which will be iterated through in the asyn result container
    #       --> async result container will call "apply"
    def map(self, func, seq, prepcode=None, workpath=None, creator=None, workpath_generator=None, condor_venv="", os_env="linux"):
        async_res = self.async_map(func, seq, prepcode, workpath, creator, workpath_generator, condor_venv=condor_venv, os_env=os_env)
        # return _AsynResultContainer(async_res)
        return async_res
        
    #preps functions into list to be called sequentially
    # will do for all jobs in the input sent to map (in seq)
    def async_map(self, func, seq, prepcode=None, workpath=None, creator=None, workpath_generator=None, condor_venv="", os_env="ldap" ):
        # seq needs to be a list of lists
        #   outer list: list of chromosomes that will each be under one async
        #   inner list: list of jobs under one chromosome
        if creator is None:
            creator = default_creator
        is_temp = False
        if workpath is None and workpath_generator is None:
            is_temp = True
            workpath = get_temp_dir()
        count = 0
        ret = []

        for chrom in seq:     
            watcher_dir = None       
            if not chrom["case_infos"]:
                continue
            chr_jobs = []
            chr_ret = []
            temp_c = 0
            for s in chrom["case_infos"]:
                if workpath_generator is None:
                    cur_wp = os.path.join(workpath, "job"+str(count))
                    if is_temp:
                        cur_wp = workpath.alias(cur_wp)
                else:
                    cur_wp = workpath_generator(func, s["args"])

                # ### Watcher directory hack....
                if watcher_dir is None and s["args"][1]["condor"] and s["args"][1].get("resource_dir") is not None:
                    # print(s["args"][1]["condor"])
                    # print(s["args"][1].get("resource_dir"))
                    # print(s["args"][1]["working_dir"])
                    # print(s["args"][0])
                    chrom_str = s["args"][0]
                    if chrom_str is None:
                        chrom_str = "DEFAULT"
                    watcher_dir = os.path.join(s["args"][1]["working_dir"], "watcher", chrom_str)
                if self.error_path is None:
                    self.error_path = os.path.join(os.path.dirname(os.path.dirname(watcher_dir)), "error")
                condor_script = self.apply(func, args=(s["args"],), prepcode=prepcode, workpath=cur_wp, creator=creator, condor_venv=condor_venv, os_env=os_env, gpus=s.get("gpus", 0), gpu_memory=s.get("gpu_memory", 0), watcher_dir=watcher_dir)
                s["job_path"] = condor_script
                s["job_id"] = os.path.basename(cur_wp)
                chr_ret.append(DAGSubResult(os.path.dirname(condor_script)))
                    
                chr_jobs.append(s)
                
                count += 1
                print("async_map job #", count)
            ret.append(_AsynResultContainer(chr_ret))


            dag_path = workpath_generator(func, (s["args"][0], {}), dag=True)
            job = self.dag_apply(chr_jobs, dag_path)
            
            self._pool.put(_async_script_job_wrapper, args=(None, job, self._timeout, self._retrynum, True, self.sub_pool_size, watcher_dir, self.error_path), timeout=None)



        return ret
    

    # (1) make contents
    # (2) dag workpath
    # (3) watching the process
    # where are the ids coming from? 
    # how about hierarchies

    """
    Assumes that there's always a 0 hierarchy
    And that the hierarchy goes in numerical order from 0
    """
    def dag_apply(self, jobs, dag_path):
        
       # (1) get the contents 
        contents = set_dag_job_hierarchy(jobs)
        # n_hierarchy = 0
        # for j in jobs:
        #     if j["hierarchy"] > n_hierarchy:
        #         n_hierarchy = j["hierarchy"]
        # parent_lines = [dict(parents=[], children=[]) for p in range(n_hierarchy)]
        # job_lines = []
        # for i, j in enumerate(jobs):
        #     job_id = j["job_id"]
        #     job_path = j["job_path"]
        #     job_lines.append("Job %s %s"%(job_id, job_path))
        #     if n_hierarchy > 0:
        #         # if it's 0 then it won't be a child but has to be parent 
        #         if j["hierarchy"] < n_hierarchy:
        #             parent_lines[j["hierarchy"]]["parents"].append(job_id)
        #         if j["hierarchy"] > 0:
        #             parent_lines[j["hierarchy"]-1]["children"].append(job_id)
        # contents = "\n".join(job_lines)

        # for parent_line in parent_lines:
        #     parents = " ".join(parent_line["parents"])
        #     children = " ".join(parent_line["children"])
        #     line = "PARENT %s CHILD %s"%(parents, children)
        #     contents+="\n"+line
        
        # (2) write the dagpath 
        os.makedirs(os.path.dirname(dag_path), exist_ok=True)
        
        with open(dag_path, 'w') as f:
            f.write(contents)

        # just make process here 
        job = new(dag_path, None) #MWW 07272020

        return job
    def join(self, timeout=None):
        return self._pool.join(timeout)
        
    def close(self):
        self._pool.close()
        
    def __enter__(self):
        return self
        
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
        return False