import traceback
import logging
import logging.handlers
import warnings

import os
import logging
import logging.handlers

DEFAULT_FORMAT = '%(asctime)s - %(levelname)s - %(module)s - %(lineno)s - %(message)s'
DEFUALT_SUFFIX = '%Y-%m-%d'

# basic file handler for logging
def get_basic_file_handler(filename,suffix=DEFUALT_SUFFIX,format=DEFAULT_FORMAT):    
    warnings.warn('not tested.')
    hdlr = logging.FileHandler(filename)
    hdlr.suffix = suffix
    formatter = logging.Formatter(format)
    hdlr.setFormatter(formatter)    
    return hdlr
    
# time rotating file handler for logging
def get_time_rotating_file_handler(filename,suffix=DEFUALT_SUFFIX,format=DEFAULT_FORMAT,when='midnight'):
    hdlr = logging.handlers.TimedRotatingFileHandler(filename,when=when)
    hdlr.suffix = suffix
    formatter = logging.Formatter(format)
    hdlr.setFormatter(formatter)
    return hdlr

# console handler for logging
def get_console_handler(format=DEFAULT_FORMAT):
    console_handler = logging.StreamHandler()
    formatter = logging.Formatter(format)
    console_handler.setFormatter(formatter)
    return console_handler
        
# custom logger 
def get_custom_logger(handlers,log_name='root',log_level=logging.DEBUG,):
    logger = logging.getLogger(log_name)
    logger.setLevel(log_level)
    for hdlr in handlers:
        logger.addHandler(hdlr)
    return logger


# '--verbose': (optional) default=0
# set the logging level
#     0: no logging (critical error only)
#     1: info level
#     2: debug level
def ga_logger(verbose):
    log = logging.getLogger()
    # if args.verbose >= 2: log.setLevel(logging.DEBUG)
    # elif args.verbose >= 1: log.setLevel(logging.INFO)
    # else: log.setLevel(logging.WARNING) # logging.ERROR / logging.CRITICAL
    
    formatter = logging.Formatter('[%(asctime)s|%(name)-10s|%(levelname)-8s|%(filename)-25s:%(lineno)-3s] %(message)s')
    ch = logging.StreamHandler()
    if verbose >= 2: log.setLevel(logging.DEBUG)
    elif verbose >= 1: log.setLevel(logging.INFO)
    else: log.setLevel(logging.WARNING) # logging.ERROR / logging.CRITICAL
    ch.setFormatter(formatter)
    log.addHandler(ch)
    return log


def default_logger():
    return ga_logger(0)
