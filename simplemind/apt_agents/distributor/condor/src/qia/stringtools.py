def prefix_line(prefix, msg, firstlineprefix=None):
    init = True
    new_msg = []
    for l in str(msg).split("\n"):
        if init and firstlineprefix is not None:
            new_msg.append(firstlineprefix+l)
        else:
            new_msg.append(prefix+l)
        init = False
    return "\n".join(new_msg)

def get_csv_compatible_string(msg):
    msg_str = str(msg)
    if str.find(msg_str, ",") or str.find(msg_str, "\""):
        msg_str = msg_str.replace("\"", "\"\"")
        return "\""+msg_str+"\""
    else:
        return msg_str

def find_unique_pattern(string, pattern, prefix="!", postfix="!"):
    pattern = pattern
    while string.find(pattern)>=0:
        pattern = prefix+pattern+postfix
    return pattern
    
def dqstr_repr(s):
    if not isinstance(s,str):
        raise ValueError()
    ret = repr(s)
    if ret[0]=="'":
        ret = ret[1:-1].replace("\\'", "'").replace('"',"\\\"")
        ret = '"%s"' % ret
    return ret