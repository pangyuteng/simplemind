
from simplemind.apt_agents.distributor.condor.src.qia.temp import get_temp_dir
from simplemind.apt_agents.distributor.condor.src.qia.threadpool import ThreadPool
from simplemind.apt_agents.distributor.condor.src.qia.exceptions import NonZeroExitCode, TimedOut, ErrorValue, NotReady
from simplemind.apt_agents.distributor.condor.src.qia.executecmd import execute_command
import simplemind.apt_agents.distributor.condor.src.qia.scriptmaker as sm
import time, os

DOCKER_INIT_CMD = "docker run --mount type=bind,source=/apps,target=/apps --mount type=bind,source=/cvib2,target=/cvib2 --mount type=bind,source=/scratch,target=/scratch --mount type=bind,source=/scratch2,target=/scratch2 --mount type=bind,source=/radraid,target=/radraid -it"


### module for semi-intellectually finding open GPU core to use in Docker ###
### if nvidia_smi is missing, then run:
###                            pip install nvidia-ml-py3 
# https://stackoverflow.com/questions/59567226/how-to-programmatically-determine-available-gpu-memory-with-tensorflow
# https://github.com/nicolargo/nvidia-ml-py3
def select_free_gpu(min_memory_gb, n_gpu=1):
    import nvidia_smi
    nvidia_smi.nvmlInit()
    n_gpu_collected = 0
    open_gpu_cores = []
    for i in range(nvidia_smi.nvmlDeviceGetCount()):
        if i > 3:
            continue
        handle = nvidia_smi.nvmlDeviceGetHandleByIndex(i)
        info = nvidia_smi.nvmlDeviceGetMemoryInfo(handle)

        memory_gb = info.free/1024/1024/1024
        # print(memory_gb)
        if memory_gb > min_memory_gb:
            n_gpu_collected+=1
            open_gpu_cores.append(i)
            if n_gpu_collected == n_gpu:
                nvidia_smi.nvmlShutdown()
                return open_gpu_cores

        # print("Total memory:", info.total)
        # print("Free memory:", info.free)
        # print("Used memory:", info.used)

    nvidia_smi.nvmlShutdown()

    return None


class DockerPool:
    def __init__(self, poolsize=None, workernum=None, error_callback=None, timeout=None, retrynum=1, single_job=False):
        if poolsize == 1:
            single_job = True
        self.return_func = None
        if not single_job:
            workernum = poolsize
            print("DOCKERPOOL: poolsize:", poolsize, "workernum", workernum)
            try:
                self._pool = ThreadPool(parallelnum=workernum, poolsize=poolsize, error_callback=error_callback)
            except:
                # TODO: use retrynum here for while loop
                i = 0
                finished = False
                while i < retrynum or finished: 
                    i+=1
                    print("ThreadPool init failed, trying again...")
                    time.sleep(5)

                    self._pool = ThreadPool(parallelnum=workernum, poolsize=poolsize, error_callback=error_callback)
                    finished = True
            self._timeout = timeout
            self._retrynum = retrynum
        else:
            print("DOCKERPOOL: Single-job non-pool version")
        self.single_job = single_job

    def set_timeout(self, timeout):
        self._timeout = timeout
    def set_return_func(self, return_func): 
        self.return_func = return_func
        # TODO: Implement return function to pair up with map, so that the value returned is not always the status (0) 
        # Theoretically would a function that would wrap the "exe_cmd" in map and then have an extra line that runs _get_evaluation, and returns that
        return 
    # condor_venv doubles as the name as the docker image of choice
    def map(self, func, seq, prepcode=None, workpath=None, creator=None, workpath_generator=None, condor_venv="", os_env="ldap"):
        if not condor_venv:
            condor_venv = "registry.cvib.ucla.edu/deep_med:debug_file_lock"

        is_temp = False
        if workpath is None and workpath_generator is None:
            is_temp = True
            workpath = get_temp_dir()

        count = 0
        ret = []
        # printer = []
        for s in seq:
            input_prepcode = prepcode 
            if workpath_generator is None:
                cur_wp = os.path.join(workpath, "job"+str(count))
                if is_temp:
                    cur_wp = workpath.alias(cur_wp)
            else:
                cur_wp = workpath_generator(func, s)
            ### TODO: Future improvement is to make this generalizable
            args=(s,)
            ## Assume s[1] is the input param
            if int(s[1].get("gpus", 0)) > 0:
                if float(s[1].get("gpu_memory", 0)) > 0:
                    free_gpu_cores = select_free_gpu(float(s[1]["gpu_memory"]), n_gpu=int(s[1]["gpus"]))
                    if free_gpu_cores is None:
                        raise("No local GPU cores are available and/or sufficient to run your process locally.")
                    if prepcode is not None:
                        if isinstance(prepcode, str):
                            input_prepcode = [prepcode, ]
                        else: 
                            input_prepcode = list(prepcode)
                    else:
                        input_prepcode = []
                    input_prepcode.append('os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"')
                    input_prepcode.append('os.environ["CUDA_VISIBLE_DEVICES"]="%s"'%(",".join([str(gpu) for gpu in free_gpu_cores])))
                      

            kwargs ={}
            obj = sm.make(cur_wp, input_prepcode, func, *args, **kwargs)

            exe_cmd = " ".join([DOCKER_INIT_CMD, condor_venv, "python", obj.script])
            print(exe_cmd)
            if self.return_func is None:
                exe_cmd = exe_cmd.split(" ")
            else:
                exe_cmd = [exe_cmd, s] 
            ret.append(exe_cmd)
            # ret.append(self.apply(func, args=(s,), prepcode=prepcode, workpath=cur_wp, creator=creator, condor_venv=condor_venv, os_env=os_env))
            #this is being limited by the thread count
            count += 1
            print("Docker threading job #", count)
            # printer.append("this isn't working.")
        if self.single_job:
            print("Mapping.>>>>>>>", len(ret))
            # map(print, printer)
            if self.return_func is not None:
                return map(self.return_func, ret)
            return map(execute_command, ret)
        if self.return_func is not None:
            # input(">>>> > > >> >> >> > >> Sending with return function!!!!")
            return self._pool.map(self.return_func, ret)
        return self._pool.map(execute_command, ret)
    def __del__(self):
        self.close()
    def join(self, timeout=None):
        if not self.single_job:
            return self._pool.join(timeout)
        
    def close(self):
        if not self.single_job:
            self._pool.close()
        
    def __enter__(self):
        return self
        
    def __exit__(self, exc_type, exc_val, exc_tb):
        if not self.single_job:
            self.close()
        return False
    # def _map(self, func, seq, prepcode=None, workpath=None, creator=None, workpath_generator=None, condor_venv="", os_env="ldap"):
    #     async_res = self.async_map(func, seq, prepcode, workpath, creator, workpath_generator, condor_venv=condor_venv, os_env=os_env)
    #     return _AsynResultContainer(async_res)
    # def apply(self, func, args=None, kwargs=None, timeout=None, prepcode=None, workpath=None, creator=None, condor_venv="", os_env="ldap"):
    #     job = _ScriptJob(workpath, prepcode, func, args, kwargs, creator, condor_venv=condor_venv, os_env=os_env) #MWW 07272020

    # def async_map(self, func, seq, prepcode=None, workpath=None, creator=None, workpath_generator=None, condor_venv="", os_env="ldap" ):
    #     if creator is None:
    #         creator = default_creator
    #     is_temp = False
    #     if workpath is None and workpath_generator is None:
    #         is_temp = True
    #         workpath = get_temp_dir()
    #     count = 0
    #     ret = []

    #     for s in seq:
    #         if workpath_generator is None:
    #             cur_wp = os.path.join(workpath, "job"+str(count))
    #             if is_temp:
    #                 cur_wp = workpath.alias(cur_wp)
    #         else:
    #             cur_wp = workpath_generator(func, s)
    #         ret.append(self.apply(func, args=(s,), prepcode=prepcode, workpath=cur_wp, creator=creator, condor_venv=condor_venv, os_env=os_env))
    #         #this is being limited by the thread count
    #         count += 1
    #         print("async_map job #", count)
    #     return ret



