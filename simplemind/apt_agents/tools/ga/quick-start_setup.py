"""

This template follows Youngwon's and Morgan's default structure:

[exp_version]
 ㄴ condor_[exp_version]
 ㄴ experiment_[exp_version]
 ㄴ ga_checkpoint
 ㄴ ga_resource
 ㄴ miu_model_[exp_version]
 ㄴ miu_resource
 ㄴ result_[exp_version]
 ㄴ summary_[exp_version]





"""
import yaml, os, shutil, pandas as pd


def main(template):
    with open(template, 'r') as f:
        experiment_info = yaml.load(f, Loader=yaml.FullLoader)

    exp_version = experiment_info["exp_version"]
    exp_base = experiment_info["exp_base"]

    ### Make Directories ###
    base_dir = os.path.join(exp_base, exp_version)
    os.makedirs(base_dir, exist_ok=True)

    condor_dir = os.path.join(base_dir, "condor")        # condor directory
    experiment_dir = os.path.join(base_dir, "experiment")# i.e. working directory
    sm_model_dir = os.path.join(base_dir, "sm_model")  # allow this to point to somewhere else if specified?
    results_dir = os.path.join(base_dir, "result")       # storing results
    summary_dir = os.path.join(base_dir, "summary")      # storing summary files during analysis
    checkpoint_dir = os.path.join(base_dir, "ga_checkpoint")            # storing checkpoints
    ga_resource_dir = os.path.join(base_dir, "ga_resource")             # if you want to run GA
    miu_resource_dir = os.path.join(base_dir, "miu_resource")           # if you want to run standalone MIU

    dirs_to_make = [condor_dir, experiment_dir, sm_model_dir, results_dir, summary_dir, checkpoint_dir, ga_resource_dir, miu_resource_dir]
    [os.makedirs(dir_, exist_ok=True) for dir_ in dirs_to_make]

    ### for the experiment ("working directory") files ###
    if experiment_info.get("experiment") is not None:
        for cnn_node, cnn_case_list in experiment_info["experiment"].items():
            os.makedirs(os.path.join(experiment_dir, cnn_node), exist_ok=True)
            shutil.copyfile(cnn_case_list, os.path.join(experiment_dir, cnn_node, "train_list.csv"))

    if experiment_info.get("ga_train_list") is not None:
        ga_train_list = os.path.join(experiment_dir, "ga_train_list.csv")
        shutil.copyfile(experiment_info["ga_train_list"], ga_train_list)
        ga_train_df = pd.read_csv(ga_train_list)
        ga_train_df["results_dir"] = results_dir
        ga_train_df["working_dir"] = experiment_dir
        ga_train_df["resource_dir"] = ga_resource_dir
        if experiment_info.get("dataset") is not None:
            ga_train_df["dataset"] = experiment_info["dataset"]
        ga_train_df["gpus"] = 0
        ga_train_df["gpu_memory"] = 0
        ga_train_df["hierarchy"] = 3



        for i, row in ga_train_df.iterrows():
            if i > 2:
                break
            ga_train_df["hierarchy"][i] = i
        
        ga_train_df.to_csv(ga_train_list)
        



    ### for the GA resource files
    for resource_file in experiment_info.get("ga_resource", []):
        shutil.copyfile(resource_file, os.path.join(ga_resource_dir, os.path.basename(resource_file)))

    ### for the MIU resource files
    for resource_file in experiment_info.get("miu_resource", []):
        shutil.copyfile(resource_file, os.path.join(miu_resource_dir, os.path.basename(resource_file)))

    ### for the model
    model = experiment_info.get("model", "")
    if model:
        if not os.path.exists(os.path.join(sm_model_dir, os.path.basename(os.path.dirname(model)))):
            shutil.copytree(os.path.dirname(model), os.path.join(sm_model_dir, os.path.basename(os.path.dirname(model))) )
            with open(os.path.join(sm_model_dir, os.path.basename(os.path.dirname(model)), "README.md"), 'w') as f:
                f.write("Original source: %s"%model)

    tb_run_template = os.path.join(experiment_info["code_base"], "examples/ga/template/launch_tb.sh")
    with open(tb_run_template, 'r') as f:
        contents = f.read()

    contents = contents%(experiment_info["tb"]["port"])

    with open(os.path.join(base_dir, "launch_tb.sh"), 'w') as f:
        f.write(contents)

    ### for the configuration ###
    if experiment_info.get("conf", ""):
        shutil.copyfile(experiment_info["conf"], os.path.join(base_dir, "ga_conf.yml"))


    ### for the ga run file ###
    entry_point = os.path.join(experiment_info["code_base"], "simplemind", "apt_agents", "ga_entry.py")

    run_ga_template = os.path.join(experiment_info["code_base"],"examples/ga/template/run_ga.sh")
    with open(run_ga_template, 'r') as f:
        contents = f.read()

    contents = contents%(entry_point, exp_version, exp_base, "%s/%s"%(os.path.basename(os.path.dirname(model)), os.path.basename(model)))

    with open(os.path.join(base_dir, "run_ga.sh"), 'w') as f:
        f.write(contents)

    print("Directory: %s"%base_dir)




from argparse import ArgumentParser
if __name__=="__main__":
    parser = ArgumentParser(description="Setting up GA experiment directory")
    parser.add_argument("template", help="input csv")
    args = parser.parse_args()
    
    main(args.template)







