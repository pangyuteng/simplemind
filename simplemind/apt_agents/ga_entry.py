"""ga entry point
"""


import os
from functools import partial
from argparse import ArgumentParser
# from conf.default_conf import EXEC_CHECKPOINT, CONDOR_PATH

# import __ga__
from simplemind.apt_agents.optimizer.ga.src.core.ga_main import main ###
from simplemind.apt_agents.optimizer.ga.src.core.ga_reader import load_input_ref, _gen_input, _gen_output, _gen_ref, get_ga_param, BaseResultsLoader as results_loader
### this one might work when we integrate GA -- for now it doesn't ###
# from simplemind import __qia__
# from simplemind.engine.src.ga.ga_main import main ###
# from simplemind.engine.src.ga.ga_reader import load_input_ref, _gen_input, _gen_output, _gen_ref, get_ga_param, BaseResultsLoader as results_loader
import shutil

"""
TODO: Log files for LCS
Log files:
    - For LIDC the results directory would be:
        [gene]\case_dir\seg
        [gene]\case_dir\log
    - For LCS
        case_dir\qi_raw\cad_[gene]
        case_dir\log\cad_[gene]
"""
"""
If you want to change an input... param_input will be from ``load_input_ref`` func
"""
## Runs MIU 
def ga_miu(chrom_str, param_input, outpath, log_file=None,):
    from simplemind import sm
    print(sm.runner.__code__.co_varnames)
    import sys
    print(sys.path)
    # from simplemind import __qia__
    if chrom_str is not None and not chrom_str.strip('0'):
        chrom_str = None
    if not os.path.exists(os.path.join(outpath, "solution_info.txt")) or param_input.get("rerun", False):
        sm_args = dict(image_path=param_input["image_file"], output_dir=outpath, sm_model=param_input["model"])
        extra = []
        if chrom_str is not None:
            sm_args["chromosome"] = chrom_str
            # extra.append("-c")
            # extra.append(chrom_str)
        # if param_input.get("roi_dir") is not None:    #MWW 09162020
        #     extra.append("-r")
        #     extra.append(param_input["roi_dir"])
        if param_input.get("working_dir") is not None:    #MWW 09162020
            sm_args["working_directory"] = param_input.get("working_dir")
            # os.makedirs(param_input["working_dir"], exist_ok=True)
            # extra.append("-d")
            # extra.append(param_input["working_dir"])
        if param_input.get("resource_dir", ""):    #MWW 09102021
            sm_args["user_resource_directory"] = param_input.get("resource_dir")
            # extra.append("-u")
            # extra.append(param_input["resource_dir"])
        if param_input.get("condor", False) and param_input.get("resource_dir", ""):
            ### if condor is used and resource_dir specified, then assume that watcher directory is needed
            dir_chrom_str = chrom_str
            if chrom_str is None:
                dir_chrom_str = "DEFAULT"
            chrom_watcher_dir = os.path.join(param_input["working_dir"], "watcher", dir_chrom_str)
            # extra.append("-w")
            # extra.append(chrom_watcher_dir)
            sm_args["watcher"] = chrom_watcher_dir

        if param_input.get("skip") is not None:
            if param_input["skip"].get("train_screenshots", False):
                # extra.append("-it")
                sm_args["skip_png_training"] = True
            if param_input["skip"].get("pred_screenshots", False):
                # extra.append("-it")
                sm_args["skip_png_prediction"] = True
            if param_input["skip"].get("tensorboard", False):
                sm_args["skip_tensorboard"] = True
                # extra.append("-t")
        # if param_input.get("cpu_preprocessing_only", False):
        #     extra.append("-p")
        if param_input.get("rerun", False):
            # extra.append("-f")
            sm_args["force_overwrite"] = True
        if log_file is not None:
            os.makedirs(os.path.dirname(log_file), exist_ok=True)
        # miu.segment(
        #     param_input["image_file"], 
        #     outpath=outpath, 
        #     extra=extra if extra else None, 
        #     model=param_input["model"], 
        #     senn=param_input.get("senn", False),        #temporary for now
        #     os_env=param_input.get("os_env", "linux"),  #temporary for now
        #     log_file=log_file,
        #     compact=True,
        # )
        sm.runner(**sm_args
            # image_path=param_input["image_file"], 
            # output_dir=outpath, 
            # extra=extra if extra else None, 
            # sm_model=param_input["model"], 
            # log_file=log_file,
            # compact=True,
        )

    return outpath

def parallel_helper(args):
    condor_only = False
    condor = False
    parallel_misc_n = int(args.parallel_misc_n)                  # how many parallel jobs run for misc tasks 
    if args.parallel_backup is not None:
        parallel_scheme = (args.parallel, args.parallel_backup)
    else:
        parallel_scheme = (args.parallel, args.parallel)
        if args.parallel is not None and args.parallel=="condor":
            condor_only=True
            
    if args.parallel is not None:
        if args.parallel=="condor":
            condor = True
        parallel_dag_n = 1                                  # size of DAG pool (ie number of parallel chrs being run)
        parallel_sub_dag_n = 5                             # how many jobs per dag (ie per chromosome) are being run
        if args.parallel_dag_n is not None:
            parallel_dag_n = int(args.parallel_dag_n)
        elif args.condor_job_count is not None:             # backwards compatibility
            parallel_dag_n = int(args.condor_job_count)
        if args.parallel_sub_dag_n is not None:
            parallel_sub_dag_n = int(args.parallel_sub_dag_n)
    else:
        parallel_scheme = ("docker_sequential", "docker_sequential")
        parallel_dag_n = 1
        parallel_sub_dag_n = 1
    return dict(parallel_scheme=parallel_scheme, parallel_dag_n=parallel_dag_n, parallel_sub_dag_n=parallel_sub_dag_n, parallel_misc_n=parallel_misc_n, condor_only=condor_only, condor=condor)

if __name__=="__main__":
    parser = ArgumentParser(description="Evaluate a single gene")
    parser.add_argument("case_csv", help="a csv containing cases to be evaluated")
    parser.add_argument("model", action="store", help="custom model file")
    parser.add_argument("--no_chrom", action="store_true", help="run MIU without chromosome input", default=False)
    parser.add_argument("--chrom", action="store", help="gene to use", default=None)
    parser.add_argument("--ga_conf", action="store", help="configuration for GA evolution", default=None)
    parser.add_argument("--checkpoint", help="checkpoint")
    parser.add_argument("--condor_path", action="store", help="optional path for Condor logs", default=None)
    parser.add_argument("--error_path", action="store", help="optional path for error reporting", default=None)
    # parser.add_argument("--parallel", action="store", help="('condor_docker_threading', 'condor_only', 'docker_sequential', 'docker_threading' )", default=None)
    parser.add_argument("--parallel", action="store", help="('condor', 'docker' )", default=None)
    parser.add_argument("--parallel_backup", action="store", help="('condor', 'docker', 'docker_sequential' -- by default it will be the same as parallel)", default=None)
    parser.add_argument("--parallel_dag_n", action="store", help="number of chromosomes run in parallel", default=None)
    parser.add_argument("--parallel_sub_dag_n", action="store", help="number of jobs per chromosome run in parallel", default=None)
    parser.add_argument("--parallel_misc_n", action="store", help="number of jobs to run local misc tasks", default=15)
    parser.add_argument("--p", action="store_true", help="CPU only preprocessing in MIU", default=False)
    parser.add_argument("--compact", action="store_true", help="cleans up for non-HOF cases", default=False)
    parser.add_argument("--os_env", action="store", help="('ad', 'ldap', 'linux')", default="linux")
    parser.add_argument("--canary_subset", action="store", help="series to initially test if gene has good enough performance", default=None)
    parser.add_argument("--condor_timeout", action="store", help="time limit for processing each chromosome, in seconds", type=int, default=None)
    parser.add_argument("--condor_venv", action="store", help="optional virtual environment path. mainly for Windows Conda support.", default="")
    parser.add_argument("--condor_job_count", action="store", help="LEGACY::: number of condor jobs", default=None)
    args = parser.parse_args()
    
    parallel = parallel_helper(args)

    ga_param = get_ga_param(conf_ga=args.ga_conf, gene=args.chrom, no_gene=args.no_chrom)
    ga_param["compact"] = args.compact
    senn = False
    Canary = None
    canary_obj = None

    evaluate_task, compile_function, results_loader, _gen_output, _gen_ref, Canary =    (ga_param.get("evaluate_task"), 
                                                                                        ga_param.get("compile_function"),
                                                                                        ga_param.get("results_loader", results_loader),
                                                                                        ga_param.get("_gen_output", _gen_output),
                                                                                        ga_param.get("_gen_ref", _gen_ref),
                                                                                        ga_param.get("Canary"),
                                                                                        )

    input_func = partial(_gen_input, model_file=args.model, senn=senn, os_env=args.os_env, skip=ga_param.get("skip"), parallel=parallel, cpu_preprocessing_only=args.p)

    case_inputs = load_input_ref(args.case_csv, input_func, _gen_ref)

    if Canary is not None:
        if args.canary_subset is not None:
            canary_subset = load_input_ref(args.canary_subset, input_func, _gen_ref)
            ga_param["canary_params"]["canary_data"] = canary_subset
            canary_obj = Canary(**ga_param["canary_params"])

    print("Imported chromosomes:",ga_param.get("init_genes"))
    computing_management = dict(parallel=parallel, os_env=args.os_env, condor_workpath=args.condor_path, error_path=args.error_path, timeout=args.condor_timeout, condor_venv=args.condor_venv,)
    main(case_inputs, args.checkpoint, ga_param, _gen_output, ga_miu, evaluate_task, compile_function, results_loader, computing_management=computing_management, canary_obj=canary_obj)
    
