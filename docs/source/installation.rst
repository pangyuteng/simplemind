.. highlight:: shell

######################################
Installation
######################################


.. Stable release
.. ^^^^^^^^^^^^^^^^^^^^^^

.. Someday...


************************************************
Using Docker (recommanded)
************************************************

    .. code-block:: bash

        # clone git repo
        git clone https://gitlab.com/sm-ai-team/simplemind.git
        cd simplemind

        # build Docker image
        docker build -t sm_release:latest .

        # run docker container
        docker run -it -u $(id -u):$(id -g) -v $PWD:/workdir -w /workdir sm_release:latest bash

        # test installation
        python -c "import simplemind; print(simplemind.__version__)"


************************************************
From PyPI (under construction)
************************************************

    .. code-block:: bash
        
        # for python 3.x
        $ pip3 install simplemind

************************************************
From sources (under construction)
************************************************

    .. code-block:: bash
        
        # for python 3.x
        $ pip3 install git+https://gitlab.com/sm-ai-team/simplemind.git