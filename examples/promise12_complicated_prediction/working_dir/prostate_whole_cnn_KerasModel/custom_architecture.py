"""Three-dimensional CNN architectures for kidney

Method
------
get_cnn_arch
    CNN architecture descriptor
    for loading a custom architecture


Custom Architectures
--------------------
get_3d_kidney_model_v0
    3D U-Net model v0 for kidney
```
pip install git+https://www.github.com/keras-team/keras-contrib.git
"""
import sys
from keras.models import Model
from keras.layers import Input
from keras.layers import Activation, BatchNormalization
from keras.layers.convolutional import Conv3DTranspose
from keras.layers import Dropout
from keras.layers import Conv3D
from keras.layers.convolutional import MaxPooling3D
from keras.layers.merge import Concatenate
# from keras_contrib.layers.normalization.instancenormalization import InstanceNormalization
from keras.layers import Layer, InputSpec
from keras import initializers, regularizers, constraints
from keras import backend as K

class InstanceNormalization(Layer): #Stolen from https://github.com/keras-team/keras-contrib/blob/master/keras_contrib/layers/normalization/instancenormalization.py
    """Instance normalization layer.
    Normalize the activations of the previous layer at each step,
    i.e. applies a transformation that maintains the mean activation
    close to 0 and the activation standard deviation close to 1.
    # Arguments
        axis: Integer, the axis that should be normalized
            (typically the features axis).
            For instance, after a `Conv2D` layer with
            `data_format="channels_first"`,
            set `axis=1` in `InstanceNormalization`.
            Setting `axis=None` will normalize all values in each
            instance of the batch.
            Axis 0 is the batch dimension. `axis` cannot be set to 0 to avoid errors.
        epsilon: Small float added to variance to avoid dividing by zero.
        center: If True, add offset of `beta` to normalized tensor.
            If False, `beta` is ignored.
        scale: If True, multiply by `gamma`.
            If False, `gamma` is not used.
            When the next layer is linear (also e.g. `nn.relu`),
            this can be disabled since the scaling
            will be done by the next layer.
        beta_initializer: Initializer for the beta weight.
        gamma_initializer: Initializer for the gamma weight.
        beta_regularizer: Optional regularizer for the beta weight.
        gamma_regularizer: Optional regularizer for the gamma weight.
        beta_constraint: Optional constraint for the beta weight.
        gamma_constraint: Optional constraint for the gamma weight.
    # Input shape
        Arbitrary. Use the keyword argument `input_shape`
        (tuple of integers, does not include the samples axis)
        when using this layer as the first layer in a Sequential model.
    # Output shape
        Same shape as input.
    # References
        - [Layer Normalization](https://arxiv.org/abs/1607.06450)
        - [Instance Normalization: The Missing Ingredient for Fast Stylization](
        https://arxiv.org/abs/1607.08022)
    """
    def __init__(self,
                 axis=None,
                 epsilon=1e-3,
                 center=True,
                 scale=True,
                 beta_initializer='zeros',
                 gamma_initializer='ones',
                 beta_regularizer=None,
                 gamma_regularizer=None,
                 beta_constraint=None,
                 gamma_constraint=None,
                 **kwargs):
        super(InstanceNormalization, self).__init__(**kwargs)
        self.supports_masking = True
        self.axis = axis
        self.epsilon = epsilon
        self.center = center
        self.scale = scale
        self.beta_initializer = initializers.get(beta_initializer)
        self.gamma_initializer = initializers.get(gamma_initializer)
        self.beta_regularizer = regularizers.get(beta_regularizer)
        self.gamma_regularizer = regularizers.get(gamma_regularizer)
        self.beta_constraint = constraints.get(beta_constraint)
        self.gamma_constraint = constraints.get(gamma_constraint)

    def build(self, input_shape):
        ndim = len(input_shape)
        if self.axis == 0:
            raise ValueError('Axis cannot be zero')

        if (self.axis is not None) and (ndim == 2):
            raise ValueError('Cannot specify axis for rank 1 tensor')

        self.input_spec = InputSpec(ndim=ndim)

        if self.axis is None:
            shape = (1,)
        else:
            shape = (input_shape[self.axis],)

        if self.scale:
            self.gamma = self.add_weight(shape=shape,
                                         name='gamma',
                                         initializer=self.gamma_initializer,
                                         regularizer=self.gamma_regularizer,
                                         constraint=self.gamma_constraint)
        else:
            self.gamma = None
        if self.center:
            self.beta = self.add_weight(shape=shape,
                                        name='beta',
                                        initializer=self.beta_initializer,
                                        regularizer=self.beta_regularizer,
                                        constraint=self.beta_constraint)
        else:
            self.beta = None
        self.built = True

    def call(self, inputs, training=None):
        input_shape = K.int_shape(inputs)
        reduction_axes = list(range(0, len(input_shape)))

        if self.axis is not None:
            del reduction_axes[self.axis]

        del reduction_axes[0]

        mean = K.mean(inputs, reduction_axes, keepdims=True)
        stddev = K.std(inputs, reduction_axes, keepdims=True) + self.epsilon
        normed = (inputs - mean) / stddev

        broadcast_shape = [1] * len(input_shape)
        if self.axis is not None:
            broadcast_shape[self.axis] = input_shape[self.axis]

        if self.scale:
            broadcast_gamma = K.reshape(self.gamma, broadcast_shape)
            normed = normed * broadcast_gamma
        if self.center:
            broadcast_beta = K.reshape(self.beta, broadcast_shape)
            normed = normed + broadcast_beta
        return normed

    def get_config(self):
        config = {
            'axis': self.axis,
            'epsilon': self.epsilon,
            'center': self.center,
            'scale': self.scale,
            'beta_initializer': initializers.serialize(self.beta_initializer),
            'gamma_initializer': initializers.serialize(self.gamma_initializer),
            'beta_regularizer': regularizers.serialize(self.beta_regularizer),
            'gamma_regularizer': regularizers.serialize(self.gamma_regularizer),
            'beta_constraint': constraints.serialize(self.beta_constraint),
            'gamma_constraint': constraints.serialize(self.gamma_constraint)
        }
        base_config = super(InstanceNormalization, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))
"""Format for using custom architecture"""
def get_cnn_arch(cnn_arch, weight_file=None, img_shapes=(512, 512), img_channels=1):
    """CNN architecture desciptor
    
    Attributes
    ----------
    cnn_arch : str
        CNN architecture descriptor
    weight_file: str
        default=None
    img_shapes: tuple of int
        default=(256, 256) # assume 2D by default
        For 3D input, use a tuple with . For example, you can use img_shapes=(256, 256, None)
    img_channels: int
        default=1
    
    Usage
    -----
    You can add more architecture by adding new model output here.

    Examples
    --------
    ```
    import cnn_arch
    model2D = cnn_arch.get_cnn_arch('unet_5block', weight_file=None,
                                    img_shape=(320, 320), img_channels=2)
    model3D = cnn_arch.get_cnn_arch('unet_3d_arb', weight_file=None,
                                    img_shape=(320, 320), img_channels=2)
    ```
    """
    model = None
    if len(img_shapes) == 2:
        raise ValueError(f'No architecture name {cnn_arch} for 2D input. \
                            \nYou can update the cnn_arch.py script \
                            \nto use a new architecture.')
    elif len(img_shapes) == 3:
        img_slices, img_rows, img_cols = img_shapes
        if cnn_arch=='unet_3d_prostate_custom':
            model = get_3d_arb_unet_instance_norm(img_rows, img_cols, img_slices, img_channels)
        elif cnn_arch=='unet_3d_prostate_custom_small':
            model = get_3d_arb_unet_small_instance_norm(img_rows, img_cols, img_slices, img_channels)
        elif cnn_arch=='unet_3d_prostate_custom_big':
            model = get_3d_arb_unet_big_instance_norm(img_rows, img_cols, img_slices, img_channels)
        else:
            raise ValueError(f'No architecture name {cnn_arch} for 3D input. \
                               \nYou can update the cnn_arch.py script \
                               \nto use a new architecture.')

    if model and weight_file:
        model.load_weights(weight_file)
    return model

def conv3D_block(input, filters, kernel_size, strides = (1,1,1), 
                kernel_initializer='he_normal',
                data_format = 'channels_last',
                padding='same', 
                name_pf=None,
                activation=None,
                bn=True, 
                inst=False, 
                dropout = 0.):
    
    if name_pf:
        name_conv = name_pf + '_conv'
        name_bn = name_pf + '_bn'
        name_in = name_pf + '_in'
        name_act = name_pf + '_activation'
        name_dropout = name_pf + '_dropout'
    else:
        name_conv = NULL
        name_bn = NULL
        name_in = NULL
        name_act = NULL
        name_dropout = NULL

    fx = Conv3D(filters, kernel_size, strides = strides,
                kernel_initializer=kernel_initializer, 
                data_format = data_format,
                padding=padding,
                name=name_conv)(input)
    if bn: fx = BatchNormalization(name=name_bn)(fx)
    if inst: fx = InstanceNormalization(axis = -1, name=name_in)(fx)
    if activation: fx = Activation(activation = activation, name=name_act)(fx)
    if dropout: fx = Dropout(dropout, name=name_dropout)(fx)
    return fx

def conv3DTranspose_block(input, filters, kernel_size, strides = (1, 1, 1), 
                kernel_initializer='he_normal',
                data_format = 'channels_last',
                padding='same', 
                name_pf=None,
                activation=None,
                bn=True, 
                inst=False, 
                dropout = 0.):
    
    if name_pf:
        name_conv = name_pf + '_convT'
        name_bn = name_pf + '_bn'
        name_in = name_pf + '_in'
        name_act = name_pf + '_activation'
        name_dropout = name_pf + '_dropout'
    else:
        name_conv = NULL
        name_bn = NULL
        name_in = NULL
        name_act = NULL
        name_dropout = NULL

    fx = Conv3DTranspose(filters, kernel_size, strides = strides,
                kernel_initializer=kernel_initializer, 
                data_format = data_format,
                padding=padding,
                name=name_conv)(input)
    if bn: fx = BatchNormalization(name=name_bn)(fx)
    if inst: fx = InstanceNormalization(axis = -1, name=name_in)(fx)
    if activation: fx = Activation(activation = activation, name=name_act)(fx)
    if dropout: fx = Dropout(dropout, name=name_dropout)(fx)
    return fx

################################################################################################################
def get_3d_arb_unet_instance_norm(img_rows=None, img_cols=None, img_slices=None, img_channels=1):
    """
    TODO: 
    1. make the architecture works for odd number of z slices
    1. add architecture input shape in ref_config
    """
    
    # patch_size = (img_slices, img_rows, img_cols, img_channels)
    patch_size = (None, None, None, img_channels)
    bn=False
    inst=True
    dropout=0.2
    nf=8

    """Encoder"""
    x_input = Input(shape=patch_size, name='input')
    e0 = conv3D_block(x_input, filters=nf, kernel_size=(3,5,5), 
                      padding='same', kernel_initializer='he_normal', name_pf='e0',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(24,320,320)
    e1 = conv3D_block(e0, filters=nf, kernel_size=(3,5,5), strides=(1,2,2), 
                      padding='same', kernel_initializer='he_normal', name_pf='e1',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(24,160,160)
    e2 = conv3D_block(e1, filters=nf*2, kernel_size=(3,5,5),
                      padding='same', kernel_initializer='he_normal', name_pf='e2',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(24,160,160)
    e3 = conv3D_block(e2, filters=nf*4, kernel_size=(3,3,3), strides=(2,2,2),
                      padding='same', kernel_initializer='he_normal', name_pf='e3',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(12,80,80)
    e4 = conv3D_block(e3, filters=nf*4, kernel_size=(3,3,3),
                      padding='same', kernel_initializer='he_normal', name_pf='e4',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(12,80,80)
    e5 = conv3D_block(e4, filters=nf*8, kernel_size=(3,3,3), strides=(2,2,2),
                      padding='same', kernel_initializer='he_normal', name_pf='e5',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(6,40,40)
    e6 = conv3D_block(e5, filters=nf*16, kernel_size=(1,3,3),
                      padding='same', kernel_initializer='he_normal', name_pf='e6',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(6,40,40)
    e7 = conv3D_block(e6, filters=nf*16, kernel_size=(1,3,3), strides=(2,2,2),
                      padding='same', kernel_initializer='he_normal', name_pf='e7',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(3,20,20)
    ################################################################################################################
    """center : encoder to decoder"""
    etd1 = conv3D_block(e7, filters=nf*16, kernel_size=(1,5,5),
                        padding='same', kernel_initializer='he_normal', name_pf='c1',
                        activation='relu', bn=bn, inst=inst, dropout=dropout) #(3,20,20)
    etd2 = conv3D_block(etd1, filters=nf*8, kernel_size=(3,3,3),
                        padding='same', kernel_initializer='he_normal', name_pf='c2',
                        activation='relu', bn=bn, inst=inst, dropout=dropout) #(3,20,20)
    ################################################################################################################
    """Decoder"""
    d7 = conv3DTranspose_block(etd2, filters=nf*16, kernel_size=(1,3,3), strides=(2,2,2), 
                               padding='same', kernel_initializer='he_normal', name_pf='d7',
                               activation='relu', bn=bn, inst=inst, dropout=dropout) #(6,40,40)
    d7concat = Concatenate(name='d7_e6_concat')([d7, e6]) #(6,40,40)
    d6 = conv3DTranspose_block(d7concat, filters=nf*8, kernel_size=(1,3,3),
                               padding='same', kernel_initializer='he_normal', name_pf='d6',
                               activation='relu', bn=bn, inst=inst, dropout=dropout) #(6,40,40)
    d5 = conv3DTranspose_block(d6, filters=nf*8, kernel_size=(3,3,3), strides=(2,2,2),
                               padding='same', kernel_initializer='he_normal', name_pf='d5',
                               activation='relu', bn=bn, inst=inst, dropout=dropout) #(12,80,80)
    d5concat = Concatenate(name='d5_e4_concat')([d5, e4]) #(12,80,80)
    d4 = conv3DTranspose_block(d5concat, filters=nf*8, kernel_size=(3,3,3),
                               padding='same', kernel_initializer='he_normal', name_pf='d4',
                               activation='relu', bn=bn, inst=inst, dropout=dropout) #(12,80,80)
    d3 = conv3DTranspose_block(d4, filters=nf*8, kernel_size=(3,3,3), strides=(2,2,2),
                               padding='same', kernel_initializer='he_normal', name_pf='d3',
                               activation='relu', bn=bn, inst=inst, dropout=dropout) #(24,160,160)
    d3concat = Concatenate(name='d3_e2_concat')([d3, e2]) #(24,160,160)
    d2 = conv3DTranspose_block(d3concat, filters=nf*4, kernel_size=(3,5,5),
                               padding='same', kernel_initializer='he_normal', name_pf='d2',
                               activation='relu', bn=bn, inst=inst, dropout=dropout) #(24,160,160)
    d1 = conv3DTranspose_block(d2, filters=nf*2, kernel_size=(3,5,5), strides=(1,2,2),
                               padding='same', kernel_initializer='he_normal', name_pf='d1',
                               activation='relu', bn=bn, inst=inst, dropout=dropout) #(24,320,320)
    d0 = conv3DTranspose_block(d1, filters=nf, kernel_size=(3,5,5),
                               padding='same', kernel_initializer='he_normal', name_pf='d0',
                               activation='relu', bn=bn, inst=inst, dropout=dropout) #(24,320,320)
    d0concat = Concatenate(name='d0_input_concat')([d0, x_input])
    h0 = conv3D_block(d0concat, filters=nf, kernel_size=(3,5,5), 
                      padding='same', kernel_initializer='he_normal', name_pf='h0',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(24,320,320)
    h1 = conv3D_block(h0, filters=nf, kernel_size=(3,5,5), 
                      padding='same', kernel_initializer='he_normal', name_pf='h1',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(24,320,320)
    last_h = Conv3D(filters=1, kernel_size=(3,3,3), padding='same', 
                    kernel_initializer='he_normal', name='p_conv')(h1)
    p_hat = Activation('sigmoid', name='p_hat')(last_h)

    model = Model(inputs=[x_input], outputs=[p_hat])
    # plot_model(model,to_file='3D_Unet_arb.png',show_shapes=True)
    print('----------------------------------------------------')
    print('Custom model:')
    print('----------------------------------------------------')
    model.summary()
    print('----------------------------------------------------')
    sys.stdout.flush()
    return model

################################################################################################################
def get_3d_arb_unet_big_instance_norm(img_rows=None, img_cols=None, img_slices=None, img_channels=1):
    """
    TODO: 
    1. make the architecture works for odd number of z slices
    1. add architecture input shape in ref_config
    """
    
    # patch_size = (img_slices, img_rows, img_cols, img_channels)
    patch_size = (None, None, None, img_channels)
    bn=False
    inst=True
    dropout=0.2
    nf=8

    """Encoder"""
    x_input = Input(shape=patch_size, name='input')
    e0 = conv3D_block(x_input, filters=nf, kernel_size=(3,3,3), 
                      padding='same', kernel_initializer='he_normal', name_pf='e0',
                      activation='relu', bn=bn, inst=inst, dropout=0.) #(28,512,512)
    e1 = conv3D_block(e0, filters=nf, kernel_size=(3,3,3), 
                      padding='same', kernel_initializer='he_normal', name_pf='e1',
                      activation='relu', bn=bn, inst=inst, dropout=0.1) #(28,512,512)
    e2 = conv3D_block(e1, filters=nf*2, kernel_size=(2,2,2), strides=(1,2,2), 
                      padding='same', kernel_initializer='he_normal', name_pf='e2',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(28,256,256)
    e3 = conv3D_block(e2, filters=nf*2, kernel_size=(3,3,3),
                      padding='same', kernel_initializer='he_normal', name_pf='e3',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(28,256,256)
    e4 = conv3D_block(e3, filters=nf*4, kernel_size=(2,2,2), strides=(2,2,2),
                      padding='same', kernel_initializer='he_normal', name_pf='e4',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(14,128,128)
    e5 = conv3D_block(e4, filters=nf*4, kernel_size=(3,3,3),
                      padding='same', kernel_initializer='he_normal', name_pf='e5',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(14,128,128)
    e6 = conv3D_block(e5, filters=nf*8, kernel_size=(2,2,2), strides=(2,2,2),
                      padding='same', kernel_initializer='he_normal', name_pf='e6',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(7,64,64)
    e7 = conv3D_block(e6, filters=nf*16, kernel_size=(3,3,3),
                      padding='same', kernel_initializer='he_normal', name_pf='e7',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(7,64,64)
    e8 = conv3D_block(e7, filters=nf*16, kernel_size=(2,2,2), strides=(1,2,2),
                      padding='same', kernel_initializer='he_normal', name_pf='e8',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(7,32,32)
    ################################################################################################################
    """center : encoder to decoder"""
    etd1 = conv3D_block(e8, filters=nf*16, kernel_size=(3,3,3),
                        padding='same', kernel_initializer='he_normal', name_pf='c1',
                        activation='relu', bn=bn, inst=inst, dropout=dropout) #(7,32,32)
    etd2 = conv3D_block(etd1, filters=nf*16, kernel_size=(3,3,3),
                        padding='same', kernel_initializer='he_normal', name_pf='c2',
                        activation='relu', bn=bn, inst=inst, dropout=dropout) #(7,32,32)
    ################################################################################################################
    """Decoder"""
    d7 = conv3DTranspose_block(etd2, filters=nf*16, kernel_size=(2,2,2), strides=(1,2,2), 
                               padding='same', kernel_initializer='he_normal', name_pf='d7',
                               activation='relu', bn=bn, inst=inst, dropout=dropout) #(7,64,64)
    d7concat = Concatenate(name='d7_e7_concat')([d7, e7]) #(7, 64, 64)
    d6 = conv3DTranspose_block(d7concat, filters=nf*8, kernel_size=(3,3,3),
                               padding='same', kernel_initializer='he_normal', name_pf='d6',
                               activation='relu', bn=bn, inst=inst, dropout=dropout) #(7,64,64)
    d5 = conv3DTranspose_block(d6, filters=nf*8, kernel_size=(2,2,2), strides=(2,2,2),
                               padding='same', kernel_initializer='he_normal', name_pf='d5',
                               activation='relu', bn=bn, inst=inst, dropout=dropout) #(14,128,128)
    d5concat = Concatenate(name='d5_e5_concat')([d5, e5]) #(14,128,128)
    d4 = conv3DTranspose_block(d5concat, filters=nf*8, kernel_size=(3,3,3),
                               padding='same', kernel_initializer='he_normal', name_pf='d4',
                               activation='relu', bn=bn, inst=inst, dropout=dropout) #(14,128,128)
    d3 = conv3DTranspose_block(d4, filters=nf*8, kernel_size=(2,2,2), strides=(2,2,2),
                               padding='same', kernel_initializer='he_normal', name_pf='d3',
                               activation='relu', bn=bn, inst=inst, dropout=dropout) #(28,256,256)
    d3concat = Concatenate(name='d3_e3_concat')([d3, e3]) #(28,256,256)
    d2 = conv3DTranspose_block(d3concat, filters=nf*4, kernel_size=(3,3,3),
                               padding='same', kernel_initializer='he_normal', name_pf='d2',
                               activation='relu', bn=bn, inst=inst, dropout=dropout) #(28,256,256)
    d1 = conv3DTranspose_block(d2, filters=nf*2, kernel_size=(2,2,2), strides=(1,2,2),
                               padding='same', kernel_initializer='he_normal', name_pf='d1',
                               activation='relu', bn=bn, inst=inst, dropout=dropout) #(28,512,512)
    d1concat = Concatenate(name='d1_e0_concat')([d1, e0])
    d0 = conv3DTranspose_block(d1concat, filters=nf, kernel_size=(3,3,3),
                               padding='same', kernel_initializer='he_normal', name_pf='d0',
                               activation='relu', bn=bn, inst=inst, dropout=dropout)  #(28,512,512)
    h0 = conv3D_block(d0, filters=nf, kernel_size=(3,3,3), 
                      padding='same', kernel_initializer='he_normal', name_pf='h0',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(28,512,512)
    # h1 = conv3D_block(h0, filters=nf, kernel_size=(3,3,3), 
    #                   padding='same', kernel_initializer='he_normal', name_pf='h1',
    #                   activation='relu', bn=bn, inst=inst, dropout=dropout) #(28,512,512)
    last_h = Conv3D(filters=1, kernel_size=(1,1,1), padding='same', 
                    kernel_initializer='he_normal', name='p_conv')(h0)
    p_hat = Activation('sigmoid', name='p_hat')(last_h)

    model = Model(inputs=[x_input], outputs=[p_hat])
    # plot_model(model,to_file='3D_Unet_arb.png',show_shapes=True)
    print('----------------------------------------------------')
    print('Custom model:')
    print('----------------------------------------------------')
    model.summary()
    print('----------------------------------------------------')
    sys.stdout.flush()
    return model

################################################################################################################
def get_3d_arb_unet_small_instance_norm(img_rows=None, img_cols=None, img_slices=None, img_channels=1):
    """
    TODO: 
    1. make the architecture works for odd number of z slices
    1. add architecture input shape in ref_config
    """
    
    # patch_size = (img_slices, img_rows, img_cols, img_channels)
    patch_size = (None, None, None, img_channels)
    bn=False
    inst=True
    dropout=0.2
    nf=4

    """Encoder"""
    x_input = Input(shape=patch_size, name='input')
    e0 = conv3D_block(x_input, filters=nf, kernel_size=(3,3,3), 
                      padding='same', kernel_initializer='he_normal', name_pf='e0',
                      activation='relu', bn=bn, inst=inst, dropout=0) #(24,320,320)
    e1 = conv3D_block(e0, filters=nf, kernel_size=(3,3,3), 
                      padding='same', kernel_initializer='he_normal', name_pf='e1',
                      activation='relu', bn=bn, inst=inst, dropout=0.1) #(24,320,320)
    e2 = conv3D_block(e1, filters=nf*2, kernel_size=(2,2,2), strides=(2,2,2), 
                      padding='same', kernel_initializer='he_normal', name_pf='e2',
                      activation='relu', bn=bn, inst=inst, dropout=0.) #(12,160,160)
    e3 = conv3D_block(e2, filters=nf*2, kernel_size=(3,3,3),
                      padding='same', kernel_initializer='he_normal', name_pf='e3',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(12,160,160)
    e4 = conv3D_block(e3, filters=nf*4, kernel_size=(2,2,2), strides=(2,2,2),
                      padding='same', kernel_initializer='he_normal', name_pf='e4',
                      activation='relu', bn=bn, inst=inst, dropout=0.) #(6,80,80)
    e5 = conv3D_block(e4, filters=nf*4, kernel_size=(3,3,3),
                      padding='same', kernel_initializer='he_normal', name_pf='e5',
                      activation='relu', bn=bn, inst=inst, dropout=dropout) #(6,80,80)
    e6 = conv3D_block(e5, filters=nf*16, kernel_size=(2,2,2), strides=(2,2,2),
                      padding='same', kernel_initializer='he_normal', name_pf='e6',
                      activation='relu', bn=bn, inst=inst, dropout=0.) #(3,40,40)
    ################################################################################################################
    """center : encoder to decoder"""
    etd1 = conv3D_block(e6, filters=nf*16, kernel_size=(3,3,3),
                        padding='same', kernel_initializer='he_normal', name_pf='c1',
                        activation='relu', bn=bn, inst=inst, dropout=dropout) #(3,40,40)
    ################################################################################################################
    """Decoder"""
    d7 = conv3DTranspose_block(etd1, filters=nf*16, kernel_size=(2,2,2), strides=(2,2,2), 
                               padding='same', kernel_initializer='he_normal', name_pf='d7',
                               activation=None, bn=bn, inst=inst, dropout=0.) #(6,80,80)
    d7concat = Concatenate(name='d7_e5_concat')([d7, e5]) #(6,80,80)
    d6 = conv3D_block(d7concat, filters=nf*16, kernel_size=(3,3,3),
                               padding='same', kernel_initializer='he_normal', name_pf='d6',
                               activation='relu', bn=bn, inst=inst, dropout=0.) #(6,80,80)
    d5 = conv3D_block(d6, filters=nf*16, kernel_size=(3,3,3),
                               padding='same', kernel_initializer='he_normal', name_pf='d5',
                               activation='relu', bn=bn, inst=inst, dropout=dropout) #(6,80,80)
    d4 = conv3DTranspose_block(d5, filters=nf*8, kernel_size=(2,2,2), strides=(2,2,2),
                               padding='same', kernel_initializer='he_normal', name_pf='d4',
                               activation=None, bn=bn, inst=inst, dropout=0.) #(12,160,160)
    d4concat = Concatenate(name='d4_e3_concat')([d4, e3]) #(12,160,160)
    d3 = conv3D_block(d4concat, filters=nf*8, kernel_size=(3,3,3),
                               padding='same', kernel_initializer='he_normal', name_pf='d3',
                               activation='relu', bn=bn, inst=inst, dropout=0.) #(12,160,160)
    d2 = conv3D_block(d3, filters=nf*8, kernel_size=(3,3,3),
                               padding='same', kernel_initializer='he_normal', name_pf='d2',
                               activation='relu', bn=bn, inst=inst, dropout=dropout) #(12,160,160)
    d1 = conv3DTranspose_block(d2, filters=nf*4, kernel_size=(2,2,2), strides=(2,2,2),
                               padding='same', kernel_initializer='he_normal', name_pf='d1',
                               activation=None, bn=bn, inst=inst, dropout=0.) #(24,320,320)
    d1concat = Concatenate(name='d1_e1_concat')([d1, e1]) #(24,160,160)
    d0 = conv3D_block(d1concat, filters=nf*4, kernel_size=(3,3,3),
                               padding='same', kernel_initializer='he_normal', name_pf='d0',
                               activation='relu', bn=bn, inst=inst, dropout=0.) #(24,320,320)
    h0 = conv3D_block(d0, filters=nf*4, kernel_size=(3,3,3), 
                      padding='same', kernel_initializer='he_normal', name_pf='h0',
                      activation='relu', bn=bn, inst=inst, dropout=0.1) #(24,320,320)
    last_h = Conv3D(filters=1, kernel_size=(3,1,1), padding='same', 
                    kernel_initializer='he_normal', name='p_conv')(h0)
    p_hat = Activation('sigmoid', name='p_hat')(last_h)

    model = Model(inputs=[x_input], outputs=[p_hat])
    # plot_model(model,to_file='3D_Unet_arb.png',show_shapes=True)
    print('----------------------------------------------------')
    print('Custom model:')
    print('----------------------------------------------------')
    model.summary()
    print('----------------------------------------------------')
    sys.stdout.flush()
    return model