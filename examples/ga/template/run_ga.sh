# Check /config/condor/localhost/default_linux_template.txt

# source /scratch/wasil/env/etc/profile.d/conda.sh
# conda activate deep_med
#### uses yaml, deap that's only in deep_med

export entry_point=%s
# export entry_point=/cvib2/apps/personal/youngwonchoi/project/QIA/miu_large_memory_stable/ga/evolve.py
# export version=ga_promise12_v1
export version=%s
export base_dir=%s/$version
export checkpoint=$base_dir/ga_checkpoint/ga.pk
export ga_conf=$base_dir/ga_conf.yml
export case_csv=$base_dir/experiment/ga_train_list.csv
export model_file=$base_dir/sm_model/%s
export condor_path=$base_dir/condor/pilot
export parallel=condor
# export parallel_backup=condor
export parallel_dag_n=5
export parallel_sub_dag_n=5

### Heavy loading
# results_dir in case_csv (executing MIU on cases in GA case list)
# working_dir in case_csv (preprocessing)
# condor_path 

python $entry_point $case_csv $model_file --checkpoint $checkpoint --ga_conf $ga_conf --condor_path $condor_path --parallel $parallel --parallel_dag_n $parallel_dag_n --parallel_sub_dag_n $parallel_sub_dag_n
# python $entry_point $case_csv $model_file --checkpoint $checkpoint --ga_conf $ga_conf --condor_path $condor_path --parallel $parallel --parallel_dag_n $parallel_dag_n --parallel_sub_dag_n $parallel_sub_dag_n --no_chrom