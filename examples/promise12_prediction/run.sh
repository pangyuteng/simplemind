export case_id=Case20
export sm_model=./sm_model_promise12/prostate_model
export working_directory=working_dir
export image_path=data/$case_id.mhd
export output_dir=output_$case_id
export force_overwrite=False
export log_file=log_sm_prediction_promise12_$case_id.log
export cmd="from simplemind import sm; sm.runner(image_path=\"${image_path}\", sm_model=\"${sm_model}\", \
output_dir=\"${output_dir}\", working_directory=\"${working_directory}\", \
force_overwrite=\"${force_overwrite}\")"
echo "$cmd"
docker run -it -u $(id -u):$(id -g) -v $PWD:/workdir -w /workdir smaiteam/sm-develop:latest bash -c "python -c '${cmd}' 2>&1 | tee ${log_file}"